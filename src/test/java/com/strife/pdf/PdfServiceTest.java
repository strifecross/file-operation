package com.strife.pdf;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.strife.pdf.enums.WaterMarkSpace;
import com.strife.pdf.enums.WaterMarkType;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * @author: strife
 * @date: 2023/11/26
 */
public class PdfServiceTest {

    private static final File srcFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demo.pdf");
    private static final File encryptDestFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demoEncrypt.pdf");
    private static final File decryptDestFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demoDecrypt.pdf");
    private static final File waterMarkDestFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demoWaterMark1.pdf");
    private static final String userPassword = "123456";
    private static final String ownerPassword = "admin123456";

    @Test
    public void encrypt() {
        IPdfService pdfService = new PdfService();
        pdfService.encryptPdf(srcFile, encryptDestFile, userPassword, ownerPassword);
    }

    @Test
    public void decrypt() {
        IPdfService pdfService = new PdfService();
        pdfService.decryptPdf(encryptDestFile, decryptDestFile, ownerPassword);
    }

    @Test
    public void waterMark() {
        WaterMark waterMark = new WaterMark();
        waterMark.addContent("仅限某某某XX用途使用");
        waterMark.addContent("2023年12月1日至2023年12与31日");
        waterMark.addContent("第三行水印");
        IPdfService pdfService = new PdfService();
        pdfService.waterMarkPdf(srcFile, waterMarkDestFile, waterMark);
    }

    @Test
    public void waterMarkXY() {
        WaterMark waterMark = new WaterMark();

        WaterMarkConfig config = new WaterMarkConfig();
        config.setXY(200, 500);
        config.setRotation(0f);
        waterMark.setWaterMarkConfig(config);

        waterMark.addContent("仅限某某某XX用途使用");
        waterMark.addContent("2023年12月1日至2023年12与31日");
        waterMark.addContent("第三行水印");
        IPdfService pdfService = new PdfService();
        pdfService.waterMarkPdf(srcFile, waterMarkDestFile, waterMark);
    }

    @Test
    public void waterMarkXY2() {
        WaterMark waterMark = new WaterMark();

        WaterMarkConfig config = new WaterMarkConfig();
        config.setXY(150, 200);
        config.setRotation(45f);
        config.setFitXY(false);
        waterMark.setWaterMarkConfig(config);

        waterMark.addContent("仅限某某某XX用途使用");
        waterMark.addContent("2023年12月1日至2023年12与31日");
        waterMark.addContent("第三行水印");
        IPdfService pdfService = new PdfService();
        pdfService.waterMarkPdf(srcFile, waterMarkDestFile, waterMark);
    }

    @Test
    public void waterMarkOverSpread() {
        WaterMark waterMark = new WaterMark();
        WaterMarkConfig waterMarkConfig = new WaterMarkConfig();
        waterMarkConfig.setFontSize(10f);
        waterMarkConfig.setRotation(45f);
        waterMarkConfig.setType(WaterMarkType.OVERSPREAD);
        waterMarkConfig.setBorderConfig(null);
        waterMarkConfig.setSpaceWidth(WaterMarkSpace.MULTIPLE_ONE);
        waterMarkConfig.setSpaceHeight(WaterMarkSpace.MULTIPLE_ONE_FIVE);
        waterMark.setWaterMarkConfig(waterMarkConfig);
        waterMark.addContent("仅限某某某XX用途使用");
        waterMark.addContent("2023年12月1日至2023年12与31日");
        waterMark.addContent("第三行水印");
        IPdfService pdfService = new PdfService();
        pdfService.waterMarkPdf(srcFile, waterMarkDestFile, waterMark);
    }
}
