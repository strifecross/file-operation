package com.strife.convert.util;

import com.strife.convert.service.word2pdf.AsposeConvertService;
import com.strife.convert.service.word2pdf.IConvertService;
import com.strife.convert.service.word2pdf.PoiConvertService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author: strife
 * @date: 2023/11/12
 */
class Word2PdfUtilTest {

    private static final String WORD_FILE_PATH = "F:\\file\\file-operation\\src\\test\\resources\\demo.docx";
    private static final String POI_PDF_FILE_PATH = "F:\\file\\file-operation\\src\\test\\resources\\demo_to_pdf_poi.pdf";
    private static final String ASPOSE_PDF_FILE_PATH = "F:\\file\\file-operation\\src\\test\\resources\\demo_to_pdf_aspose2.pdf";

    @Test
    void poiConvert() {
        IConvertService word2PdfService = new PoiConvertService();
        word2PdfService.word2Pdf(WORD_FILE_PATH, POI_PDF_FILE_PATH);
    }

    @Test
    void asposeConvert() {
        IConvertService word2PdfService = new AsposeConvertService();
        word2PdfService.word2Pdf(WORD_FILE_PATH, ASPOSE_PDF_FILE_PATH);
    }
}