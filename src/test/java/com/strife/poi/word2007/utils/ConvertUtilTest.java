package com.strife.poi.word2007.utils;

import com.strife.poi.word2007.entity.Person;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author mengzhenghao
 * @date 2022/8/7
 */
class ConvertUtilTest {

    private static XWPFDocument document;

    @BeforeAll
    public static void before() throws IOException {
        InputStream inputStream = ExtractorUtilTest.class.getClassLoader().getResourceAsStream("demo.docx");
        if (inputStream == null) {
            throw new IOException("文件不存在");
        }
        document = new XWPFDocument(inputStream);
    }

    @Test
    void convertToPerson() {
        List<List<String>> data = ExtractorUtil.extractorTableData(document);
        List<Person> people = ConvertUtil.convertToPerson(data, true);
        people.forEach(System.out::println);
    }
}
