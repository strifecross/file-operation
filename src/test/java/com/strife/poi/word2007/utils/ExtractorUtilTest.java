package com.strife.poi.word2007.utils;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFPictureData;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.List;

/**
 * @author mengzhenghao
 * @date 2022/8/7
 */
class ExtractorUtilTest {

    private static XWPFDocument document;

    @BeforeAll
    public static void before() throws IOException {
        InputStream inputStream = ExtractorUtilTest.class.getClassLoader().getResourceAsStream("demo.docx");
        if (inputStream == null) {
            throw new IOException("文件不存在");
        }
        document = new XWPFDocument(inputStream);
    }

    @Test
    void extractorAllCharacter() {
        System.out.println(ExtractorUtil.extractorAllCharacter(document));
    }

    @Test
    void extractorParagraph() {
        ExtractorUtil.extractorParagraph(document)
                .forEach(System.out::println);
    }

    @Test
    void extractorHeadersText() {
        ExtractorUtil.extractorHeadersText(document)
                .forEach(System.out::println);
    }

    @Test
    void extractorFootersText() {
        ExtractorUtil.extractorFootersText(document)
                .forEach(System.out::println);
    }

    @Test
    void extractorTable() {
        List<XWPFTable> xwpfTables = ExtractorUtil.extractorTable(document);
        System.out.println(xwpfTables.hashCode());
    }

    @Test
    void extractorTableData() {
        ExtractorUtil.extractorTableData(document)
                .forEach(System.out::println);
    }

    @Test
    void extractorPictures() {
        File file = new File("src/test/resources/img.jpg");
        List<XWPFPictureData> pictures = ExtractorUtil.extractorPictures(document);
        pictures.forEach(picture -> {
            byte[] data = picture.getData();
            try {
                OutputStream outputStream = new FileOutputStream(file);
                IOUtils.write(data, outputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }
}
