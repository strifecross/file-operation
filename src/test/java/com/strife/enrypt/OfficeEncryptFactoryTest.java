package com.strife.enrypt;

import com.strife.encrypt.factory.OfficeFileEncryptFactory;
import org.junit.jupiter.api.Test;

import java.io.File;

/**
 * @author: strife
 * @date: 2023/11/26
 */
public class OfficeEncryptFactoryTest {

   @Test
   void encrypt() {
       OfficeFileEncryptFactory.encrypt(new File("F:\\file\\file-operation\\src\\test\\resources\\demo.docx"),
               new File("F:\\file\\file-operation\\src\\test\\resources\\demoEncryptFactory.docx"),
               "123456");
   }

    @Test
    void decrypt() {
        OfficeFileEncryptFactory.decrypt(new File("F:\\file\\file-operation\\src\\test\\resources\\demoEncryptFactory.docx"),
                "123456");
    }

    @Test
    void onlyRead() {
        OfficeFileEncryptFactory.encrypt(new File("F:\\file\\file-operation\\src\\test\\resources\\demo.docx"),
                new File("F:\\file\\file-operation\\src\\test\\resources\\demoOnlyFactory.docx"),
                "123456");
    }
}
