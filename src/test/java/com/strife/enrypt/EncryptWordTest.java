package com.strife.enrypt;

import com.strife.encrypt.service.AbstractEncryptService;
import com.strife.encrypt.service.EncryptDocxFileService;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * @author: strife
 * @date: 2023/11/15
 */
public class EncryptWordTest {

    private static final File srcFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demo.docx");
    private static final File destFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demoEncrypt.docx");
    private static final File decryptDestFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demoDecrypt.docx");

    private static final String password = "123456";

    @Test
    public void encrypt() {
        AbstractEncryptService encryptService = new EncryptDocxFileService();
        encryptService.encryptFile(srcFile, password);
        encryptService.encryptFile(srcFile, destFile, password);
    }

    @Test
    public void onlyRead() {
        AbstractEncryptService encryptService = new EncryptDocxFileService();
        encryptService.onlyReadFile(srcFile, password);
        encryptService.onlyReadFile(srcFile, destFile, password);
    }

    @Test
    public void decrypt() throws Exception {
        AbstractEncryptService encryptService = new EncryptDocxFileService();
        XWPFDocument document = (XWPFDocument) encryptService.decrypt(destFile, password);
        OutputStream outputStream = new FileOutputStream(decryptDestFile);
        document.write(outputStream);
        outputStream.flush();
    }
}
