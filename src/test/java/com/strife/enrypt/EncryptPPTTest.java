package com.strife.enrypt;

import com.strife.encrypt.service.AbstractEncryptService;
import com.strife.encrypt.service.EncryptPPTFileService;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * @author: strife
 * @date: 2023/11/15
 */
public class EncryptPPTTest {

    private static final File srcFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demo.pptx");
    private static final File enctyptDestFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demoEncrypt.pptx");
    private static final File decryptDestFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demoDecrypt.pptx");

    private static final File onlyReadDestFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demoOnlyRead.pptx");
    private static final String password = "123456";

    @Test
    public void encrypt() {
        AbstractEncryptService encryptService = new EncryptPPTFileService();
        encryptService.encryptFile(srcFile, enctyptDestFile, password);
    }

    @Test
    public void onlyRead() {
        Assertions.assertThrowsExactly(UnsupportedOperationException.class, () -> {
            AbstractEncryptService encryptService = new EncryptPPTFileService();
            encryptService.onlyReadFile(srcFile, onlyReadDestFile, password);
        });
    }

    @Test
    public void decrypt() throws Exception {
        AbstractEncryptService encryptService = new EncryptPPTFileService();
        XMLSlideShow xmlSlideShow = (XMLSlideShow) encryptService.decrypt(enctyptDestFile, password);
        OutputStream outputStream = new FileOutputStream(decryptDestFile);
        xmlSlideShow.write(outputStream);
        outputStream.flush();
    }
}
