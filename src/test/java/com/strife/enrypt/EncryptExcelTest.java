package com.strife.enrypt;

import com.strife.encrypt.service.AbstractEncryptService;
import com.strife.encrypt.service.EncryptExcelFileService;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * @author: strife
 * @date: 2023/11/15
 */
public class EncryptExcelTest {

    private static final File srcFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demo.xlsx");
    private static final File encryptDestFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demoEncrypt.xlsx");
    private static final File decryptDestFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demoDecrypt.xlsx");
    private static final File onlyReadDestFile = new File("F:\\file\\file-operation\\src\\test\\resources\\demoOnlyRead.xlsx");
    private static final String password = "123456";

    @Test
    public void encrypt() {
        AbstractEncryptService encryptService = new EncryptExcelFileService();
        encryptService.encryptFile(srcFile, encryptDestFile, password);
    }

    @Test
    public void onlyRead() {
        AbstractEncryptService encryptService = new EncryptExcelFileService();
        encryptService.onlyReadFile(srcFile, onlyReadDestFile, password);
    }

    @Test
    public void decrypt() throws Exception {
        AbstractEncryptService encryptService = new EncryptExcelFileService();
        Workbook workbook = (Workbook) encryptService.decrypt(encryptDestFile, password);
        OutputStream outputStream = new FileOutputStream(decryptDestFile);
        workbook.write(outputStream);
        outputStream.flush();
    }
}
