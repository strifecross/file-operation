package com.strife.io;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 孟正浩
 * @date 2023-12-29
 **/
class ResourcesTest {

    private static final String RESOURCE_URL = "application.yml";

    @Test
    void getResourceURL() throws IOException {
        URL url = Resources.getResourceURL(RESOURCE_URL);
        Assert.notNull(url, "error");
    }

    @Test
    void getResourceAsStream() throws IOException {
        InputStream stream = Resources.getResourceAsStream(RESOURCE_URL);
        Assert.notNull(stream, "error");
    }


    @Test
    void getResourceAsFile() throws IOException {
        File file = Resources.getResourceAsFile(RESOURCE_URL);
        Assert.notNull(file, "error");
    }

    @Test
    void getResourceAsProperties() throws IOException {
        Properties properties = Resources.getResourceAsProperties(RESOURCE_URL);
        Assert.notNull(properties, "error");
    }
}