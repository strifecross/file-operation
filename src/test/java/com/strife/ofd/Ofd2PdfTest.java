package com.strife.ofd;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 孟正浩
 * @date 2024-05-16
 **/
class Ofd2PdfTest {

    @Test
    void ofd2Pdf() {
        String ofdPath = "C:\\Users\\mzh\\Desktop\\b.ofd";
        String pdfPath = "C:\\Users\\mzh\\Desktop\\c.pdf";
        Ofd2Pdf.ofd2Pdf(ofdPath, pdfPath);
    }

    @Test
    void pdf2Ofd() throws IOException {
        String pdfPath = "C:\\Users\\mzh\\Desktop\\a.pdf";
        String ofdPath = "C:\\Users\\mzh\\Desktop\\b.ofd";
        Ofd2Pdf.pdf2Ofd(pdfPath, ofdPath);
    }
}