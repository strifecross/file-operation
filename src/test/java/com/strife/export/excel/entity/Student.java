package com.strife.export.excel.entity;

import com.strife.export.excel.annotation.Excel;

/**
 * @description 学生类
 * @author: strife
 * @date: 2022/8/14
 */
public class Student {

    @Excel(value = "序列号", width = 500)
    private String id;

    @Excel("名称")
    private String name;

    @Excel("性别")
    private String sex;

    private Integer age;

    public Student() {
    }

    public Student(String id, String name, String sex, Integer age) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
