package com.strife.export.excel.entity;

import lombok.Data;

/**
 * @author 孟正浩
 * @date 2024/7/31
 **/
@Data
public class IncomeExpenses {

    private Double income1;
    private Double income2;
    private Double income3;
    private Double income4;
    private Double income5;
    private Double income6;
    private Double income7;
    private Double income8;
    private Double income9;
    private Double income10;
    private Double income11;
    private Double income12;
    private Double expenses1;
    private Double expenses2;
    private Double expenses3;
    private Double expenses4;
    private Double expenses5;
    private Double expenses6;
    private Double expenses7;
    private Double expenses8;
    private Double expenses9;
    private Double expenses10;
    private Double expenses11;
    private Double expenses12;
}
