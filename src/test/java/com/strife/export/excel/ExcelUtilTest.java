package com.strife.export.excel;

import com.strife.export.excel.entity.Student;
import com.strife.export.excel.service.impl.ExcelServiceImpl;
import com.strife.export.excel.util.ExcelUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @description
 * @author: strife
 * @date: 2022/8/14
 */
class ExcelUtilTest {

    @Test
    void exportExcel() throws Exception {

        List<Student> list = new ArrayList<>();
        list.add(new Student("1", "张三", "男", 18));
        list.add(new Student("2", "李四", "女", 20));
        list.add(new Student("3", "王五", "男", 24));

        HttpServletResponse response = new MockHttpServletResponse();
        ExcelUtil.exportExcel(list, Student.class, ExcelServiceImpl.class, response, "export.xls", -1);
        Assertions.assertEquals(response.getStatus(), HttpServletResponse.SC_OK);
    }
}