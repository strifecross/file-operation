package com.strife.export.excel.util;

import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.util.RandomUtil;
import com.strife.export.excel.entity.CellData;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * @author 孟正浩
 * @date 2024-05-30
 **/
class ExcelTemplateUtilTest {

    @SuppressWarnings("all")
    @Test
    void fillTemplate() throws Exception {
        List<CellData> cellDatas = new ArrayList<>();
        cellDatas.add(new CellData(0, 1, 1, "张三"));
        cellDatas.add(new CellData(0, 1, 3, "13011112222"));
        cellDatas.add(new CellData(0, 2, 1, "男"));
        cellDatas.add(new CellData(0, 2, 3, "xxxx@qq.com"));
        cellDatas.add(new CellData(0, 3, 1, "24"));
        cellDatas.add(new CellData(0, 3, 3, "这是备注……"));
        File file = new File("D:\\code\\mine\\file-operation\\src\\test\\resources\\t.xlsx");
        try (InputStream stream = ResourceUtil.getStream("template.xlsx");
             OutputStream outputStream = new FileOutputStream(file)) {
            Workbook workbook = ExcelTemplateUtil.fillTemplate(cellDatas, stream);
            workbook.write(outputStream);
        }

    }

    @SuppressWarnings("all")
    @Test
    void fillTemplateVar() throws Exception {
        Map<String, Object> varMap = mockVarMap();
        File file = new File("D:\\code\\mine\\file-operation\\src\\test\\resources\\t.xlsx");
        try (InputStream stream = ResourceUtil.getStream("templateVar.xlsx");
             OutputStream outputStream = new FileOutputStream(file)) {
            Workbook workbook = ExcelTemplateUtil.fillTemplate(varMap, stream);
            workbook.write(outputStream);
        }

    }

    private Map<String, Object> mockVarMap() {
        Map<String, Object> var = new HashMap<>(24);
        int min = 3000;
        int max = 12000;
        var.put("userName", "张三");
        var.put("date", new Date());
        var.put("income1", RandomUtil.randomInt(min, max));
        var.put("income2", RandomUtil.randomInt(min, max));
        var.put("income3", RandomUtil.randomInt(min, max));
        var.put("income4", RandomUtil.randomInt(min, max));
        var.put("income5", RandomUtil.randomInt(min, max));
        var.put("income6", RandomUtil.randomInt(min, max));
        var.put("income7", RandomUtil.randomInt(min, max));
        var.put("income8", RandomUtil.randomInt(min, max));
        var.put("income9", RandomUtil.randomInt(min, max));
        var.put("income10", RandomUtil.randomInt(min, max));
        var.put("income11", RandomUtil.randomInt(min, max));
        var.put("income12", RandomUtil.randomInt(min, max));
        var.put("expenses1", RandomUtil.randomInt(min, max));
        var.put("expenses2", RandomUtil.randomInt(min, max));
        var.put("expenses3", RandomUtil.randomInt(min, max));
        var.put("expenses4", RandomUtil.randomInt(min, max));
        var.put("expenses5", RandomUtil.randomInt(min, max));
        var.put("expenses6", RandomUtil.randomInt(min, max));
        var.put("expenses7", RandomUtil.randomInt(min, max));
        var.put("expenses8", RandomUtil.randomInt(min, max));
        var.put("expenses9", RandomUtil.randomInt(min, max));
        var.put("expenses10", RandomUtil.randomInt(min, max));
        var.put("expenses11", RandomUtil.randomInt(min, max));
        var.put("expenses12", RandomUtil.randomInt(min, max));
        return var;
    }
}