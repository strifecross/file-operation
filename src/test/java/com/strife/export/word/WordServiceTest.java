package com.strife.export.word;

import com.strife.export.word.entity.TemplateVar;
import com.strife.export.word.service.IWordService;
import com.strife.export.word.service.WordService;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author: strife
 * @date: 2023/9/16
 */
public class WordServiceTest {

    private static final String TEMPLATE_PATH;
    private static final String OUTPUT_PATH;

    static {
        TEMPLATE_PATH = "F:\\file\\file-operation\\src\\test\\resources\\template.docx";
        OUTPUT_PATH = "F:\\file\\file-operation\\src\\test\\resources\\output.docx";
    }

    @Test
    public void testRenderTemplate() {
        IWordService wordService = new WordService();
        wordService.renderTemplate(TEMPLATE_PATH, OUTPUT_PATH, mockVars());
    }

    private List<TemplateVar> mockVars() {
        return List.of(
                new TemplateVar("username", "strife"),
                new TemplateVar("日期", new SimpleDateFormat("yyyy-MM-dd").format(new Date()))
        );
    }
}
