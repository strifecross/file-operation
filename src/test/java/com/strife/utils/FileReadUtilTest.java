package com.strife.utils;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author: strife
 * @date: 2023/10/2
 */
class FileReadUtilTest {

    @Test
    public void testReadTxtFile() throws IOException {
        String path = FileReadUtil.class.getResource("/test.txt").getPath();
        String content = FileReadUtil.readTxtFile(new File(path));
        assertNotNull(content);
    }

    @Test
    public void testReadPdfFile() throws IOException {
        String path = FileReadUtil.class.getResource("/demo.pdf").getPath();
        String content = FileReadUtil.readPdfFile(new File(path));
        System.out.println(content);
        assertNotNull(content);
    }

    @Test
    public void testReadEncryptPdfFile() throws IOException {
        String path = FileReadUtil.class.getResource("/demoEncrypt.pdf").getPath();
        String content = FileReadUtil.readPdfFile(new File(path), "admin123456");
        System.out.println(content);
        assertNotNull(content);
    }
}