package com.strife.utils;

import cn.hutool.core.lang.Assert;
import org.junit.jupiter.api.Test;

import java.io.File;

/**
 * @author: strife
 * @date: 2023/11/19
 */
public class FileTypeUtilTest {

    public static File DOCX_FILE = new File("F:\\file\\file-operation\\src\\test\\resources\\demo.docx");
    public static File PDF_FILE = new File("F:\\file\\file-operation\\src\\test\\resources\\demo.pdf");
    public static File PPTX_FILE = new File("F:\\file\\file-operation\\src\\test\\resources\\demo.pptx");
    public static File XLSX_FILE = new File("F:\\file\\file-operation\\src\\test\\resources\\demo.xlsx");
    public static File TXT_FILE = new File("F:\\file\\file-operation\\src\\test\\resources\\test.txt");

    @Test
    void getFileRealType() {
        Assert.equals(FileTypeUtil.getFileRealType(DOCX_FILE), "docx");
        Assert.equals(FileTypeUtil.getFileRealType(PDF_FILE), "pdf");
        Assert.equals(FileTypeUtil.getFileRealType(PPTX_FILE), "pptx");
        Assert.equals(FileTypeUtil.getFileRealType(XLSX_FILE), "xlsx");
        Assert.equals(FileTypeUtil.getFileRealType(TXT_FILE), "txt");
    }
}
