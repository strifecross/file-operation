package com.strife.pdf;

import com.itextpdf.text.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Files;

/**
 * @author: strife
 * @date: 2023/12/10
 */
public class WaterMarkImage {

    private static final Logger logger = LoggerFactory.getLogger(WaterMarkImage.class);

    public static final float XIMGRATION = (float)(456.00 / 595.32);
    public static final float YIMGRATION = (float)(456.00 / 841.92);
    public static final float XLINERATION = (float)(123.00 / 456.00);
    public static final float YLINERATION = (float)(333.00 / 456.00);
    public static final float FONTRATION = (float)(15.00 / 456.00);

    private WaterMark waterMark;

    private WaterMarkConfig waterMarkConfig;
    private BorderConfig borderConfig;

    public WaterMarkImage(WaterMark waterMark) {
        this.waterMark = waterMark;
        this.waterMarkConfig = waterMark.getWaterMarkConfig();
        this.borderConfig = waterMarkConfig.getBorderConfig();
    }

    public Image generateImage(float dpi) throws Exception {
        if (borderConfig.getBorderWidth() == 0) {
            return null;
        }
        int imgWidth = getImgWidth(dpi);
        int imgHeight = getImgHeight();

        BufferedImage bufferedImage = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB);

        Graphics2D graphics2D = bufferedImage.createGraphics();
        // 背景设置透明
        bufferedImage = graphics2D.getDeviceConfiguration().createCompatibleImage(imgWidth, imgHeight, Transparency.TRANSLUCENT);
        graphics2D.dispose();
        graphics2D = bufferedImage.createGraphics();
        graphics2D.setColor(borderConfig.getColor());
        graphics2D.setStroke(new BasicStroke(borderConfig.getBorderWidth()));
        // 设置对线段的锯齿状边缘处理
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        int lineWidth = (int) (imgWidth - borderConfig.getBorderWidth() * 2);
        int lineHeight = (int) (imgHeight - borderConfig.getBorderWidth() * 2);
        graphics2D.draw(new Line2D.Float(borderConfig.getBorderWidth(), borderConfig.getBorderWidth(), lineWidth, borderConfig.getBorderWidth()));
        graphics2D.draw(new Line2D.Float(lineWidth, borderConfig.getBorderWidth(), lineWidth, lineHeight));
        graphics2D.draw(new Line2D.Float(lineWidth, lineHeight, borderConfig.getBorderWidth(), lineHeight));
        graphics2D.draw(new Line2D.Float(borderConfig.getBorderWidth(), lineHeight, borderConfig.getBorderWidth(), borderConfig.getBorderWidth()));
        bufferedImage = rotateImage(bufferedImage, waterMarkConfig.getRotation());
        graphics2D.dispose();

        File file = Files.createTempFile("WaterMarkImage", ".png").toFile();
        ImageIO.write(bufferedImage, "png", file);

        return Image.getInstance(file.getAbsolutePath());
    }

    /**
     * 旋转图片
     */
    private BufferedImage rotateImage(BufferedImage originalImage, float angle) {
        // 根据旋转角度计算旋转之后的图片大小，防止旋转后被裁剪
        double sin = Math.abs(Math.sin(angle));
        double cos = Math.abs(Math.cos(angle));
        int newWidth = (int) Math.round(originalImage.getWidth() * cos + originalImage.getHeight() * sin);
        int newHeight = (int) Math.round(originalImage.getWidth() * sin + originalImage.getHeight() * cos);
        newWidth = Math.max(newWidth, originalImage.getWidth());
        newHeight = Math.max(newHeight, originalImage.getHeight());
        BufferedImage rotatedImage = new BufferedImage(newWidth, newHeight, originalImage.getType());
        Graphics2D g2d = rotatedImage.createGraphics();
        g2d.setColor(Color.BLACK);
        AffineTransform at = new AffineTransform();
        at.translate((double) (newWidth - originalImage.getWidth()) / 2,
                (double) (newHeight - originalImage.getHeight()) / 2);
        at.rotate(-1 * Math.toRadians(angle),
                (double) originalImage.getWidth() / 2, (double) originalImage.getHeight() / 2);
        // 设置对线段的锯齿状边缘处理
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.drawRenderedImage(originalImage, at);
        g2d.dispose();
        return rotatedImage;
    }

    private int getImgContentX(int imgWidth, int contentWidth) {
        switch (waterMarkConfig.getAlign()) {
            case LEFT:
                return 0;
            case CENTER:
                return imgWidth / 2 - contentWidth / 2;
            case RIGHT:
                return imgWidth - contentWidth;
        }
        return 0;
    }

    private int getImgContentY(int imgHeight, int contentHeight) {
        return 0;
    }

    private int getImgWidth(float dpi) {
        if (borderConfig != null) {
            return borderConfig.getWidth();
        }
        return waterMark.getTextMaxWidth(dpi);
    }

    private int getImgHeight() {
        if (borderConfig != null) {
            return borderConfig.getHeight();
        }
        Float fontSize = waterMarkConfig.getFontSize();
        float textHeight = fontSize * waterMark.getContents().size();
        return (int) (textHeight + fontSize);
    }
}
