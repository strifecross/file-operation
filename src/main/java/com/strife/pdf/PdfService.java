package com.strife.pdf;

import com.itextpdf.awt.geom.AffineTransform;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.strife.pdf.enums.WaterMarkSpace;
import com.strife.pdf.enums.WaterMarkType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author: strife
 * @date: 2023/11/26
 */
public class PdfService implements IPdfService {

    private static final Logger logger = LoggerFactory.getLogger(PdfService.class);

    @Override
    public void encryptPdf(File srcFile, File destFile, String userPassword, String ownerPassword) {
        try (InputStream inputStream = new FileInputStream(srcFile);
             OutputStream outputStream = new FileOutputStream(destFile)) {
            PdfReader pdfReader = new PdfReader(inputStream);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, outputStream);
            // 访问者密码 拥有者密码 访问者权限 拥有者权限
            /* 权限参数
                PdfWriter.ALLOW_MODIFY_CONTENTS 允许打印,编辑，复制，签名 加密级别：40-bit-RC4
                PdfWriter.ALLOW_COPY **允许复制，签名 不允许打印，编辑 加密级别：40-bit-RC ***
                PdfWriter.ALLOW_MODIFY_ANNOTATIONS 允许打印,编辑，复制，签名 加密级别：40-bit-RC4
                PdfWriter.ALLOW_FILL_IN 允许打印,编辑，复制，签名 加密级别：40-bit-RC4
                PdfWriter.ALLOW_SCREENREADERS 允许打印,编辑，复制，签名 加密级别：40-bit-RC4
                PdfWriter.ALLOW_ASSEMBLY 允许打印,编辑，复制，签名 加密级别：40-bit-RC4
                PdfWriter.EMBEDDED_FILES_ONLY 允许打印,编辑，复制，签名 加密级别：40-bit-RC4
                PdfWriter.DO_NOT_ENCRYPT_METADATA 允许打印,编辑，复制，签名 加密级别：40-bit-RC4
                PdfWriter.ENCRYPTION_AES_256 允许打印,编辑，复制，签名 加密级别：256-bit-AES
                PdfWriter.ENCRYPTION_AES_128 允许打印,编辑，复制，签名 加密级别：128-bit-AES
                PdfWriter.STANDARD_ENCRYPTION_128 允许打印,编辑，复制，签名 加密级别：128-bit-RC4
                PdfWriter.STANDARD_ENCRYPTION_40 允许打印,编辑，复制，签名 加密级别：40-bit-RC4
             */
            pdfStamper.setEncryption(userPassword.getBytes(StandardCharsets.UTF_8),
                    ownerPassword.getBytes(StandardCharsets.UTF_8),
                    PdfWriter.ALLOW_COPY, PdfWriter.ENCRYPTION_AES_128);
            pdfStamper.close();
            pdfReader.close();
        } catch (IOException | DocumentException e) {
            logger.error("pdf加密失败", e);
        }

    }

    @Override
    public void decryptPdf(File srcFile, File destFile, String password) {
        try (InputStream inputStream = new FileInputStream(srcFile);
             OutputStream outputStream = new FileOutputStream(destFile)) {
            ReaderProperties properties = new ReaderProperties()
                    .setOwnerPassword(password.getBytes(StandardCharsets.UTF_8));
            PdfReader pdfReader = new PdfReader(properties, inputStream);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, outputStream);
            pdfStamper.close();
            pdfReader.close();
        } catch (IOException | DocumentException e) {
            logger.error("Pdf解密失败", e);
        }
    }

    @Override
    public void waterMarkPdf(File srcFile, File destFile, WaterMark waterMark) {
        try (InputStream inputStream = new FileInputStream(srcFile);
             OutputStream outputStream = new FileOutputStream(destFile)) {
            PdfReader pdfReader = new PdfReader(inputStream);
            if (pdfReader.getNumberOfPages() < 1) {
                pdfReader.close();
                return;
            }
            PdfStamper pdfStamper = new PdfStamper(pdfReader, outputStream);
            WaterMarkConfig waterMarkConfig = waterMark.getWaterMarkConfig();
            BaseFont font = waterMarkConfig.getFont();
            PdfContentByte overContent = pdfStamper.getOverContent(1);
            float pdfWidth = overContent.getPdfDocument().getPageSize().getWidth();
            float pdfHeight = overContent.getPdfDocument().getPageSize().getHeight();
            // 分辨率
            float dpi = getDpi(pdfReader, pdfWidth, pdfHeight);
            Image image = null;
            if (waterMarkConfig.getBorderConfig() != null) {
                image = new WaterMarkImage(waterMark).generateImage(dpi);
            }


            for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
                PdfContentByte content = pdfStamper.getOverContent(i);
                content.setGState(waterMarkConfig.getPdfGState());
                content.beginText();
                content.setColorFill(waterMarkConfig.getFontColor());
                Float fontSize = waterMarkConfig.getFontSize();
                if (image != null) {
                    float imageWidth = image.getWidth();
                    float imageHeight = image.getHeight();
                    float imgX = waterMarkConfig.getX() - imageWidth / 2 ;
                    float imgY = waterMarkConfig.getY() - imageHeight / 2;
                    if (waterMarkConfig.isFitXY()) {
                        imgX = handlePosX(imgX, imageWidth);
                        imgY = handlePosY(imgY, imageHeight);
                    }
                    image.setAbsolutePosition(imgX, imgY);
                    content.addImage(image);
                }

                PdfTemplate template = content.createTemplate(PageSize.A4.getWidth(), PageSize.A4.getHeight());

                BorderConfig borderConfig = waterMarkConfig.getBorderConfig();
                int textWidth, textHeight;
                if (waterMarkConfig.getBorderConfig() != null) {
                    textWidth = borderConfig.getWidth();
                    textHeight = borderConfig.getHeight();
                } else {
                    textWidth = waterMark.getTextMaxWidth(dpi);
                    textHeight = waterMark.getTextHeight();
                }

                float templateX = waterMarkConfig.getX() - textWidth / 2;
                float templateY = waterMarkConfig.getY() - textHeight / 2;
                if (waterMarkConfig.isFitXY()) {
                    templateX = handlePosX(templateX, textWidth);
                    templateY = handlePosX(templateY, textHeight);
                }
                Rectangle columnRectangle;
                if (WaterMarkType.SINGLE.equals(waterMarkConfig.getType())) {
                    columnRectangle = new Rectangle(templateX, templateY,
                            templateX + textWidth, templateY + textHeight);
                } else {
                    columnRectangle = new Rectangle(0, 0, textWidth, textHeight * 2);
                }
                ColumnText columnText = new ColumnText(template);
                columnText.setSimpleColumn(columnRectangle);
                // 设置行段落间距
                // columnText.setExtraParagraphSpace(UnitUtil.px2Point(100, dpi));
                Font wordFont = new Font(font, fontSize, Font.NORMAL, waterMarkConfig.getFontColor());
                for (String waterMarkContent : waterMark.getContents()) {
                    Paragraph paragraph = new Paragraph(waterMarkContent, wordFont);
                    paragraph.setAlignment(waterMarkConfig.getAlign().ordinal());
                    columnText.addElement(paragraph);
                }
                columnText.go();
                content.saveState();
                content.concatCTM(AffineTransform.getRotateInstance(Math.toRadians(waterMarkConfig.getRotation()),
                        waterMarkConfig.getX(), waterMarkConfig.getY()));
                if (WaterMarkType.SINGLE.equals(waterMarkConfig.getType())) {
                    content.addTemplate(template, 0, 0);
                } else {
                    int wSpace = waterMarkConfig.getSpaceWidth().getSpaceWidth(textWidth);
                    int hSpace = waterMarkConfig.getSpaceHeight().getSpaceHeight(textHeight);
                    for (float x = -2 * textWidth; x < pdfWidth + textWidth; x += wSpace) {
                        for (float y = -2 * textHeight; y < pdfHeight + textHeight; y += hSpace) {
                            content.addTemplate(template, x, y);
                        }
                    }
                }
                content.restoreState();
                content.endText();
            }
            pdfStamper.close();
            pdfReader.close();
        } catch (Exception e) {
            logger.error("Pdf添加水印失败", e);
        }
    }

    private float handlePosX(float x, float width) {
        if (x < 0) {
            return 0;
        }
        return Math.min(x, PageSize.A4.getWidth() - width);
    }

    private float handlePosY(float y, float height) {
        if (y < 0) {
            return 0;
        }
        return Math.min(y, PageSize.A4.getHeight() - height);
    }

    /**
     * 获取pdf的分辨率
     */
    private float getDpi(PdfReader reader, float pdfWidth, float pdfHeight) {
        int rotation = reader.getPageRotation(1);

        int dpi;
        if (rotation == 90 || rotation == 270) {
            dpi = Math.round(pdfHeight / 11);
        } else {
            dpi = Math.round(pdfWidth / 8.5f);
        }
        return dpi;
    }
}
