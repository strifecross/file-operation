package com.strife.pdf.enums;

/**
 * 水印类型
 *
 * @author: strife
 * @date: 2023/12/17
 */
public enum WaterMarkType {

    /**
     * 单个水印
     */
    SINGLE,
    /**
     * 全屏水印
     */
    OVERSPREAD
}
