package com.strife.pdf.enums;

/**
 * 水印内容对其方式
 *
 * @author: strife
 * @date: 2023/12/10
 * @see com.itextpdf.text.Element#ALIGN_LEFT
 */
public enum Align {

    LEFT,
    CENTER,
    RIGHT


}
