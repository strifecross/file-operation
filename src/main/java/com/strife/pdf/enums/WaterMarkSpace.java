package com.strife.pdf.enums;

/**
 * 水印间距
 *
 * @author: strife
 * @date: 2023/12/17
 */
public enum WaterMarkSpace {

    /**
     * 一倍
     */
    MULTIPLE_ONE {
        @Override
        public int getSpaceWidth(int width) {
            return width;
        }

        @Override
        public int getSpaceHeight(int height) {
            return height;
        }
    },

    /**
     * 1.5倍
     */
    MULTIPLE_ONE_FIVE {
        @Override
        public int getSpaceWidth(int width) {
            return (int) (width * 1.5);
        }

        @Override
        public int getSpaceHeight(int height) {
            return (int) (height * 1.5);
        }
    },

    /**
     * 二倍
     */
    MULTIPLE_TWO {
        @Override
        public int getSpaceWidth(int width) {
            return width * 2;
        }

        @Override
        public int getSpaceHeight(int height) {
            return height * 2;
        }
    };

    public abstract int getSpaceWidth(int width);

    public abstract int getSpaceHeight(int height);
}
