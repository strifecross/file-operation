package com.strife.pdf.util;

/**
 * @author: strife
 * @date: 2023/12/17
 */
public class UnitUtil {

    /**
     * pdf中点转换为px
     *
     * @param point 点数
     * @param dpi   分辨率
     * @return 转换为px返回
     */
    public static float point2Px(float point, float dpi) {
        return point * dpi / 72;
    }

    /**
     * pdf中px转换为点
     *
     * @param px  px值
     * @param dpi 分辨率
     * @return 转换为px返回
     */
    public static float px2Point(float px, float dpi) {
        return px * 72 / dpi;
    }
}
