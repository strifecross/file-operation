package com.strife.pdf;

import java.awt.*;

/**
 * @author: strife
 * @date: 2023/12/10
 */
public class BorderConfig {

    private int width = 300;
    private int height = 100;
    private float borderWidth = 2f;

    private Color color = Color.RED;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
