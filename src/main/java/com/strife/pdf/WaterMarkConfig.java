package com.strife.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfGState;
import com.strife.pdf.enums.Align;
import com.strife.pdf.enums.WaterMarkSpace;
import com.strife.pdf.enums.WaterMarkType;

/**
 * pdf水印配置
 *
 * @author: strife
 * @date: 2023/12/10
 */
public class WaterMarkConfig {

    /**
     * 水印类型
     */
    private WaterMarkType type = WaterMarkType.SINGLE;

    /**
     * 字体
     */
    private BaseFont font;

    /**
     * 字体大小
     */
    private Float fontSize;

    /**
     * 字体颜色
     */
    private BaseColor fontColor;

    /**
     * 对齐方式
     */
    private Align align;

    /**
     * 不透明度
     */
    private Float fillOpacity;

    /**
     * 水印中心点x轴位置
     */
    private Float x;

    /**
     * 水印中心点y轴位置
     */
    private Float y;

    /**
     * 旋转角度
     */
    private Float rotation;

    /**
     * 水印是否展示外边框
     */
    private BorderConfig borderConfig;

    /**
     * 自动调整XY轴位置，防止水印超出文档范围
     */
    private boolean fitXY = true;

    /**
     * 水印横向间距（水印类型为 {@link WaterMarkType#OVERSPREAD} 时生效）
     */
    private WaterMarkSpace spaceWidth = WaterMarkSpace.MULTIPLE_ONE;

    /**
     * 水印纵向间距（水印类型为 {@link WaterMarkType#OVERSPREAD} 时生效）
     */
    private WaterMarkSpace spaceHeight = WaterMarkSpace.MULTIPLE_ONE;

    /**
     * 默认使用STSong-Light字体，字体大小30，水印垂直、水平居中显示，
     * 透明度0.3，旋转45度，显示红色外边框
     */
    public WaterMarkConfig() {
        this(20f, Align.CENTER, 0.4f, PageSize.A4.getWidth() / 2, PageSize.A4.getHeight() / 2,
                45f, new BorderConfig());
    }

    public WaterMarkConfig(Float fontSize, Align align, Float fillOpacity, Float x, Float y,
                           Float rotation, BorderConfig borderConfig) {
        this(null, fontSize, null, align, fillOpacity, x, y, rotation, borderConfig);
        this.font = getDefaultFont();
        this.fontColor = getDefaultColor();
    }

    public WaterMarkConfig(BaseFont font, Float fontSize, BaseColor fontColor, Align align, Float fillOpacity, Float x, Float y,
                           Float rotation, BorderConfig borderConfig) {
        this.font = font;
        this.fontSize = fontSize;
        this.fontColor = fontColor;
        this.align = align;
        this.fillOpacity = fillOpacity;
        this.x = x;
        this.y = y;
        this.rotation = rotation;
        this.borderConfig = borderConfig;
    }

    /**
     * 默认字体，依赖 itext-asian 字体包
     */
    public BaseFont getDefaultFont() {
        try {
            return BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.EMBEDDED);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 默认字体颜色，红色
     */
    public BaseColor getDefaultColor() {
        return BaseColor.RED;
    }

    /**
     * 默认水印边框为红色
     */
    public BorderConfig getDefaultBorderConfig() {
        BorderConfig config = new BorderConfig();

        return config;
    }

    public PdfGState getPdfGState() {
        PdfGState pdfGState = new PdfGState();
        if (fillOpacity != null) {
            pdfGState.setFillOpacity(fillOpacity);
        }
        return pdfGState;
    }
    public void setXY(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public BaseFont getFont() {
        return font;
    }

    public void setFont(BaseFont font) {
        this.font = font;
    }

    public Float getFontSize() {
        return fontSize;
    }

    public void setFontSize(Float fontSize) {
        this.fontSize = fontSize;
    }

    public BaseColor getFontColor() {
        return fontColor;
    }

    public void setFontColor(BaseColor fontColor) {
        this.fontColor = fontColor;
    }

    public Align getAlign() {
        return align;
    }

    public void setAlign(Align align) {
        this.align = align;
    }

    public Float getFillOpacity() {
        return fillOpacity;
    }

    public void setFillOpacity(Float fillOpacity) {
        this.fillOpacity = fillOpacity;
    }

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }

    public Float getRotation() {
        return rotation;
    }

    public void setRotation(Float rotation) {
        this.rotation = rotation;
    }

    public BorderConfig getBorderConfig() {
        return borderConfig;
    }

    public void setBorderConfig(BorderConfig borderConfig) {
        this.borderConfig = borderConfig;
    }

    public boolean isFitXY() {
        return fitXY;
    }

    public void setFitXY(boolean fitXY) {
        this.fitXY = fitXY;
    }

    public WaterMarkType getType() {
        return type;
    }

    public void setType(WaterMarkType type) {
        this.type = type;
    }

    public WaterMarkSpace getSpaceWidth() {
        return spaceWidth;
    }

    public void setSpaceWidth(WaterMarkSpace spaceWidth) {
        this.spaceWidth = spaceWidth;
    }

    public WaterMarkSpace getSpaceHeight() {
        return spaceHeight;
    }

    public void setSpaceHeight(WaterMarkSpace spaceHeight) {
        this.spaceHeight = spaceHeight;
    }
}
