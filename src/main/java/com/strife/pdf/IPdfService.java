package com.strife.pdf;

import java.io.File;

/**
 * @author: strife
 * @date: 2023/11/26
 */
public interface IPdfService {

    /**
     * Pdf文件加密
     *
     * @param srcFile       源文件
     * @param destFile      加密后文件
     * @param userPassword  访问者密码
     * @param ownerPassword 拥有者密码
     */
    void encryptPdf(File srcFile, File destFile, String userPassword, String ownerPassword);

    /**
     * Pdf文件解密
     *
     * @param srcFile  加密文件
     * @param destFile 解密后文件
     * @param password 拥有者密码
     */
    void decryptPdf(File srcFile, File destFile, String password);

    /**
     * Pdf文件添加水印
     *
     * @param srcFile         源文件
     * @param destFile        加水印后的文件
     * @param waterMark       水印
     */
    void waterMarkPdf(File srcFile, File destFile, WaterMark waterMark);
}
