package com.strife.pdf;

import com.deepoove.poi.util.UnitUtils;
import com.itextpdf.text.pdf.BaseFont;
import com.strife.pdf.util.UnitUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: strife
 * @date: 2023/12/10
 */
public class WaterMark {

    private WaterMarkConfig waterMarkConfig = new WaterMarkConfig();

    /**
     * 水印内容
     */
    private List<String> contents;

    public WaterMark() {
    }

    public void addContent(String content) {
        if (contents == null) {
            contents = new ArrayList<>();
        }
        contents.add(content);
    }

    public int getTextMaxWidth(float dpi) {
        if (contents == null || contents.isEmpty()) {
            return 0;
        }
        // 点的宽度
        float widthInPoints = 0;
        BaseFont font = waterMarkConfig.getFont();
        for (String content : contents) {
            widthInPoints = Math.max(widthInPoints, font.getWidthPoint(content, waterMarkConfig.getFontSize()));
        }
        // 转换为px
        float px = UnitUtil.point2Px(widthInPoints, dpi);
        // 适当放宽
        return (int) (px + px / 5);
    }

    public int getTextHeight() {
        if (contents == null || contents.isEmpty()) {
            return 0;
        }
        float fontSize = waterMarkConfig.getFontSize();
        BaseFont font = waterMarkConfig.getFont();
        float capHeight = font.getFontDescriptor(BaseFont.CAPHEIGHT, fontSize);
        float ascent = font.getFontDescriptor(BaseFont.ASCENT, fontSize);
        float descent = font.getFontDescriptor(BaseFont.DESCENT, fontSize);

        float fontHeight = ascent - descent;
        // 考虑有段间距，这里返回2倍
        return (int) (fontHeight * contents.size() * 2);
    }

    public List<String> getContents() {
        return contents;
    }

    public void setContents(List<String> contents) {
        this.contents = contents;
    }

    public WaterMarkConfig getWaterMarkConfig() {
        return waterMarkConfig;
    }

    public void setWaterMarkConfig(WaterMarkConfig waterMarkConfig) {
        this.waterMarkConfig = waterMarkConfig;
    }
}
