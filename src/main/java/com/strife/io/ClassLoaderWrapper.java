package com.strife.io;

import java.io.InputStream;
import java.net.URL;

/**
 * @author 孟正浩
 * @date 2023-12-29
 **/
public class ClassLoaderWrapper {

    private ClassLoader defaultClassLoader;
    private final ClassLoader systemClassLoader;

    public ClassLoaderWrapper() {
        this.systemClassLoader = ClassLoader.getSystemClassLoader();
    }

    /**
     * 读取resources文件为InputStream
     *
     * @param resource 资源路径
     * @return 返回InputStream
     */
    public InputStream getResourceAsStream(String resource) {
        return getResourceAsStream(resource, getClassLoaders(null));
    }

    /**
     * 读取resources文件为InputStream
     *
     * @param resource    资源路径
     * @param classLoader 指定类加载器
     * @return 返回InputStream
     */
    public InputStream getResourceAsStream(String resource, ClassLoader classLoader) {
        return getResourceAsStream(resource, getClassLoaders(classLoader));
    }

    /**
     * 读取resources文件为URL
     *
     * @param resource 资源路径
     * @return 返回URL
     */
    public URL getResourceAsURL(String resource) {
        return getResourceAsURL(resource, getClassLoaders(null));
    }

    /**
     * 读取resource文件为URL
     *
     * @param resource    资源路径
     * @param classLoader 指定类加载器
     * @return 返回URL
     */
    public URL getResourceAsURL(String resource, ClassLoader classLoader) {
        return getResourceAsURL(resource, getClassLoaders(classLoader));
    }

    /**
     * 读取resources文件为InputStream
     *
     * @param resource     资源路径
     * @param classLoaders 类加载器
     * @return 返回InputStream
     */
    public InputStream getResourceAsStream(String resource, ClassLoader[] classLoaders) {
        if (classLoaders == null || classLoaders.length == 0) {
            return null;
        }
        InputStream stream;
        for (ClassLoader classLoader : classLoaders) {
            if (classLoader == null) {
                continue;
            }
            stream = classLoader.getResourceAsStream(resource);
            if (stream == null) {
                //加 "/" 再取一次
                stream = classLoader.getResourceAsStream("/" + resource);
            }

            if (stream != null) {
                return stream;
            }
        }
        return null;
    }

    /**
     * 读取resources文件为URL
     *
     * @param resource     资源路径
     * @param classLoaders 类加载器
     * @return 返回URL
     */
    public URL getResourceAsURL(String resource, ClassLoader[] classLoaders) {
        if (classLoaders == null || classLoaders.length == 0) {
            return null;
        }
        URL url;
        for (ClassLoader classLoader : classLoaders) {
            if (classLoader == null) {
                continue;
            }
            url = classLoader.getResource(resource);
            if (url == null) {
                //加 "/" 再取一次
                url = classLoader.getResource("/" + url);
            }
            if (url != null) {
                return url;
            }
        }
        return null;
    }

    ClassLoader[] getClassLoaders(ClassLoader classLoader) {
        return new ClassLoader[]{
                classLoader,
                defaultClassLoader,
                Thread.currentThread().getContextClassLoader(),
                getClass().getClassLoader(),
                systemClassLoader};
    }

    public void setDefaultClassLoader(ClassLoader defaultClassLoader) {
        this.defaultClassLoader = defaultClassLoader;
    }

    public ClassLoader getDefaultClassLoader() {
        return defaultClassLoader;
    }

    public ClassLoader getSystemClassLoader() {
        return systemClassLoader;
    }
}
