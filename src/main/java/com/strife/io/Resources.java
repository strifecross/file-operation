package com.strife.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/**
 * 读取resources文件夹下的文件
 *
 * @author 孟正浩
 * @date 2023-12-29
 **/
public class Resources {

    private static ClassLoaderWrapper classLoaderWrapper = new ClassLoaderWrapper();

    /**
     * 读取resource文件为URL
     *
     * @param resource    资源路径
     * @return 返回URL
     */
    public static URL getResourceURL(String resource) throws IOException {
        return getResourceURL(resource, null);
    }

    /**
     * 读取resource文件为URL
     *
     * @param resource    资源路径
     * @param classLoader 指定类加载器
     * @return 返回URL
     */
    public static URL getResourceURL(String resource, ClassLoader classLoader) throws IOException {
        URL url = classLoaderWrapper.getResourceAsURL(resource, classLoader);
        if (url == null) {
            throw new IOException("Could not find resource " + resource);
        }
        return url;
    }

    /**
     * 读取resources文件为InputStream
     *
     * @param resource    资源路径
     * @return 返回InputStream
     */
    public static InputStream getResourceAsStream(String resource) throws IOException {
        return getResourceAsStream(resource, null);
    }

    /**
     * 读取resource文件为InputStream
     *
     * @param resource    资源路径
     * @param classLoader 指定类加载器
     * @return 返回InputStream
     */
    public static InputStream getResourceAsStream(String resource, ClassLoader classLoader) throws IOException {
        InputStream stream = classLoaderWrapper.getResourceAsStream(resource, classLoader);
        if (stream == null) {
            throw new IOException("Could not find resource " + resource);
        }
        return stream;
    }

    /**
     * 读取resource文件为File
     *
     * @param resource    资源路径
     * @return 返回File
     */
    public static File getResourceAsFile(String resource) throws IOException {
        return new File(getResourceURL(resource).getFile());
    }

    /**
     * 读取resource文件为File
     *
     * @param resource    资源路径
     * @param classLoader 指定类加载器
     * @return 返回File
     */
    public static File getResourceAsFile(String resource, ClassLoader classLoader) throws IOException {
        return new File(getResourceURL(resource, classLoader).getFile());
    }

    /**
     * 读取resource文件为Properties
     *
     * @param resource    资源路径
     * @return 返回File
     */
    public static Properties getResourceAsProperties(String resource) throws IOException {
        Properties properties = new Properties();
        try (InputStream inputStream = getResourceAsStream(resource, null)) {
            properties.load(inputStream);
        }
        return properties;
    }

    /**
     * 读取resource文件为Properties
     *
     * @param resource    资源路径
     * @param classLoader 指定类加载器
     * @return 返回File
     */
    public static Properties getResourceAsProperties(String resource, ClassLoader classLoader) throws IOException {
        Properties properties = new Properties();
        try (InputStream inputStream = getResourceAsStream(resource, classLoader)) {
            properties.load(inputStream);
        }
        return properties;
    }

    /**
     * 获取默认类加载器
     */
    public ClassLoader getDefaultClassLoader() {
        return classLoaderWrapper.getDefaultClassLoader();
    }

    /**
     * 设置默认类加载器
     */
    public void setDefaultClassLoader(ClassLoader classLoader) {
        classLoaderWrapper.setDefaultClassLoader(classLoader);
    }
}
