package com.strife.enums;

import com.strife.encrypt.factory.OfficeFileEncryptFactory;
import com.strife.encrypt.service.AbstractEncryptService;
import com.strife.encrypt.service.EncryptDocxFileService;
import com.strife.encrypt.service.EncryptExcelFileService;
import com.strife.encrypt.service.EncryptPPTFileService;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: strife
 * @date: 2023/11/26
 */
public enum OfficeEncryptServiceEnum {

    DOCX("docx", new EncryptDocxFileService()),
    XLSX("xlsx", new EncryptExcelFileService()),
    PPTX("pptx", new EncryptPPTFileService());

    /**
     * 文件类型
     */
    private final String suffix;

    /**
     * 加密服务
     */
    private final AbstractEncryptService encryptService;

    OfficeEncryptServiceEnum(String suffix, AbstractEncryptService encryptService) {
        this.suffix = suffix;
        this.encryptService = encryptService;
    }

    private static Map<String, AbstractEncryptService> serviceMap = new HashMap<>();

    static {
        for (OfficeEncryptServiceEnum encryptServiceEnum : OfficeEncryptServiceEnum.values()) {
            serviceMap.put(encryptServiceEnum.suffix, encryptServiceEnum.encryptService);
        }
    }

    public static AbstractEncryptService getOfficeEncryptService(String fileType) {
        AbstractEncryptService officeEncryptService = serviceMap.get(fileType);
        if (officeEncryptService == null) {
            throw new RuntimeException("暂不支持处理该种各类型文件");
        }
        return officeEncryptService;
    }
}
