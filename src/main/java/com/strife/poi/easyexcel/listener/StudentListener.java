package com.strife.poi.easyexcel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ConverterUtils;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.fastjson.JSON;
import com.strife.poi.easyexcel.entity.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * @author mengzhenghao
 * @date 2022/7/17
 */
public class StudentListener implements ReadListener<Student> {

    private static final Logger logger = LoggerFactory.getLogger(StudentListener.class);

    /**
     * 单次缓存的数据量
     */
    public static final int BATCH_COUNT = 100;
    /**
     * 临时存储
     */
    private List<Student> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

    @Override
    public void invoke(Student student, AnalysisContext context) {
        cachedDataList.add(student);
        if (cachedDataList.size() >= BATCH_COUNT) {
            saveData(cachedDataList);
            cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    private void saveData(List<Student> cachedDataList) {
        logger.debug("{}条数据开始存数据库", cachedDataList.size());
        for (Student student : cachedDataList) {
            logger.info(JSON.toJSONString(student));
        }
        logger.debug("存数据库完成");
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        saveData(cachedDataList);
        logger.debug("所有数据解析完成");
    }

    @Override
    public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
        Map<Integer, String> stringMap = ConverterUtils.convertToStringMap(headMap, context);
        logger.info("解析到一条头数据:{}", JSON.toJSONString(stringMap));
    }
}
