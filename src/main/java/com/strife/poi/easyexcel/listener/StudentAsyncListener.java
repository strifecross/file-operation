package com.strife.poi.easyexcel.listener;

import com.strife.poi.easyexcel.core.AsyncImportParam;
import com.strife.poi.easyexcel.entity.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author mengzhenghao
 * @date 2022/7/17
 */
public class StudentAsyncListener extends AsyncListener<Student> {

    private static final Logger logger = LoggerFactory.getLogger(StudentAsyncListener.class);

    /**
     * 单次缓存的数据量
     */
    public static final int BATCH_COUNT = 100;

    public StudentAsyncListener(AsyncImportParam asyncImportParam) {
        super(asyncImportParam);
    }


    /**
     * 获取批次大小
     *
     * @return 一次保存条数
     */
    @Override
    protected int batchSize() {
        return BATCH_COUNT;
    }

    @Override
    protected void saveData(List<Student> cachedDataList) {
        // logger.debug("{}条数据开始存数据库", cachedDataList.size());
        // for (Student student : cachedDataList) {
        //     logger.info(JSON.toJSONString(student));
        // }
        // logger.debug("存数据库完成");
    }
}
