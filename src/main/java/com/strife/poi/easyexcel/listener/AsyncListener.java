package com.strife.poi.easyexcel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ConverterUtils;
import com.alibaba.fastjson.JSON;
import com.strife.poi.easyexcel.core.AsyncImportManager;
import com.strife.poi.easyexcel.core.AsyncImportParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 单sheet导入
 *
 * @author 孟正浩
 * @date 2024-05-10
 **/
public abstract class AsyncListener<T> implements ReadListener<T> {
    private static final Logger logger = LoggerFactory.getLogger(AsyncListener.class);

    /**
     * 保存一批数据，用于之后保存
     */
    protected List<T> CACHE_DATA = new ArrayList<>();

    /**
     * Excel总行数，不一定准确，用于计算导入进度，不做他用
     */
    protected Integer totalNums;
    /**
     * 实际数据条数
     */
    protected Integer realTotalNums;
    /**
     * 批次数
     */
    protected Integer batch;
    /**
     * 已保存批次数
     */
    protected Integer saveBatch;

    protected AsyncImportParam asyncImportParam;

    public AsyncListener(AsyncImportParam asyncImportParam) {
        this.asyncImportParam = asyncImportParam;
    }

    /**
     * 获取批次大小
     *
     * @return 一次保存条数
     */
    protected abstract int batchSize();

    @Override
    public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
        Map<Integer, String> stringMap = ConverterUtils.convertToStringMap(headMap, context);
        logger.info("解析到一条头数据:{}", JSON.toJSONString(stringMap));
        if (totalNums == null) {
            realTotalNums = 0;
            totalNums = context.readSheetHolder().getApproximateTotalRowNumber();
            batch = totalNums % batchSize() == 0 ? totalNums / batchSize() : (totalNums / batchSize()) + 1;
            saveBatch = 0;
            AsyncImportManager.INSTANCE.updateProgressBarInfo(asyncImportParam.hashCode(), context.readSheetHolder().getSheetNo(), 0D);
        }
    }

    @Override
    public void invoke(T t, AnalysisContext context) {
        realTotalNums++;
        CACHE_DATA.add(t);
        if (CACHE_DATA.size() >= batchSize()) {
            saveData(CACHE_DATA);
            CACHE_DATA = new ArrayList<>();
            saveBatch++;
            if (saveBatch < batch) {
                AsyncImportManager.INSTANCE.updateProgressBarInfo(asyncImportParam.hashCode(), context.readSheetHolder().getSheetNo(), (double)saveBatch / batch * 100D);
            }
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        saveData(CACHE_DATA);
        logger.debug("所有数据解析完成,总条数: {}", realTotalNums);
        AsyncImportManager.INSTANCE.updateProgressBarInfo(asyncImportParam.hashCode(), context.readSheetHolder().getSheetNo(), 100D);
    }

    /**
     * 入库
     *
     * @param cachedDataList 一批数据
     */
    protected abstract void saveData(List<T> cachedDataList);


}
