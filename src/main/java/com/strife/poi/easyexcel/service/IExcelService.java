package com.strife.poi.easyexcel.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author mengzhenghao
 * @date 2022/7/17
 */
public interface IExcelService {

    /**
     * 读取Excel文件
     */
    void simpleReadExcel(MultipartFile file) throws IOException;

    /**
     * 异步导入
     */
    int asyncImport(MultipartFile file) throws IOException;
}
