package com.strife.poi.easyexcel.service;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.listener.PageReadListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.fastjson.JSON;
import com.strife.poi.easyexcel.core.AsyncImportManager;
import com.strife.poi.easyexcel.core.AsyncImportParam;
import com.strife.poi.easyexcel.core.ImportProgressInfo;
import com.strife.poi.easyexcel.entity.Student;
import com.strife.poi.easyexcel.listener.StudentAsyncListener;
import com.strife.poi.easyexcel.listener.StudentListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author mengzhenghao
 * @date 2022/7/17
 */
@Service
public class EasyExcelService implements IExcelService {

    private static final Logger logger = LoggerFactory.getLogger(EasyExcelService.class);

    @Override
    public void simpleReadExcel(MultipartFile file) throws IOException {
        try {
            //readMethod1(file);
            //readMethod2(file);
            //readMethod3(file);
            readMethod4(file);
        } catch (IOException e) {
            logger.error("excel文件读取失败，原因：{}", e.getMessage());
            throw e;
        }
    }

    /**
     * 异步导入
     */
    @Override
    public int asyncImport(MultipartFile file) throws IOException {
        AsyncImportParam param = new AsyncImportParam();
        param.setInputStream(file.getInputStream());
        param.setFunction(asyncImportParam -> {
            ExcelReader excelReader = EasyExcel.read(asyncImportParam.getInputStream(), Student.class, new StudentAsyncListener(asyncImportParam)).build();
            ImportProgressInfo progressInfo = new ImportProgressInfo();
            progressInfo.setSheetNums(excelReader.excelExecutor().sheetList().size());
            progressInfo.setProgressMap(new HashMap<>());
            AsyncImportManager.SHEET_PROGRESS.put(asyncImportParam.hashCode(), progressInfo);
            for (ReadSheet readSheet : excelReader.excelExecutor().sheetList()) {
                excelReader.read(readSheet);
            }
        });
        AsyncImportManager.INSTANCE.offer(param);
        return param.hashCode();
    }

    /**
     * 每100条读取一次，只读取一个sheet
     */
    private void readMethod1(MultipartFile file) throws IOException {
        // 读取第一个sheet文件流会自动关闭
        // 每次会读取100条数据，然后返回过来，直接调用使用数据就行
        EasyExcel.read(file.getInputStream(), Student.class, new PageReadListener<Student>(dataList -> {
            for (Student student : dataList) {
                logger.info("读取到一条数据： {}", JSON.toJSONString(student));
            }
        })).sheet().doRead();
    }


    /**
     * 读取第一个sheet文件流会自动关闭
     */
    private void readMethod2(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(), Student.class, new StudentListener()).sheet().doRead();
    }

    /**
     * 读取指定sheet
     */
    private void readMethod3(MultipartFile file) throws IOException {
        // 一个文件一个reader
        try (ExcelReader excelReader = EasyExcel.read(file.getInputStream(), Student.class, new StudentListener()).build()) {
            // 构建一个sheet 这里可以指定名字或者no
            ReadSheet readSheet = EasyExcel.readSheet(0).build();
            excelReader.read(readSheet);
        }
    }

    /**
     * 读取多个sheet
     */
    private void readMethod4(MultipartFile file) throws IOException {
        //读取全部sheet
        EasyExcel.read(file.getInputStream(), Student.class, new StudentListener()).doReadAll();

        try (final ExcelReader excelReader = EasyExcel.read(file.getInputStream()).build()) {
            // 这里为了简单 所以注册了 同样的head 和Listener 自己使用功能必须不同的Listener
            ReadSheet readSheet1 = EasyExcel.readSheet(0)
                    .head(Student.class)
                    .registerReadListener(new StudentListener())
                    .build();
            ReadSheet readSheet2 = EasyExcel.readSheet(1)
                    .head(Student.class)
                    .registerReadListener(new StudentListener())
                    .build();
            // 这里注意 一定要把sheet1 sheet2 一起传进去，不然有个问题就是03版的excel 会读取多次，浪费性能
            excelReader.read(readSheet1, readSheet2);
        }
    }


}
