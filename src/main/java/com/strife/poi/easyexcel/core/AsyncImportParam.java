package com.strife.poi.easyexcel.core;

import lombok.Data;

import java.io.InputStream;

/**
 * @author 孟正浩
 * @date 2024-05-10
 **/
@Data
public class AsyncImportParam {

    private AsyncImportFunction function;
    private InputStream inputStream;
}
