package com.strife.poi.easyexcel.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayDeque;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author 孟正浩
 * @date 2024-05-10
 **/
public class AsyncImportManager {
    private static final Logger logger = LoggerFactory.getLogger(AsyncImportManager.class);

    private static ArrayDeque<AsyncImportParam> DEQUE = new ArrayDeque<>();
    public static Map<Integer, AsyncImportParam> WAIT = new ConcurrentHashMap<>();
    public static Map<Integer, AsyncImportParam> IMPORTING = new ConcurrentHashMap<>();
    public static Map<Integer, AsyncImportParam> FINISHED = new ConcurrentHashMap<>();

    public static Map<Integer, ImportProgressInfo> SHEET_PROGRESS = new ConcurrentHashMap<>();
    public static Map<Integer, Double> PROGRESS = new ConcurrentHashMap<>();

    private static Semaphore semaphore = new Semaphore(10);

    public static AsyncImportManager INSTANCE = new AsyncImportManager();

    public AsyncImportManager() {
        new Holder();
    }

    public int offer(AsyncImportParam param) {
        int hashCode = param.hashCode();
        DEQUE.offer(param);
        WAIT.put(hashCode, param);
        return hashCode;
    }

    public AsyncImportParam poll() {
        AsyncImportParam param = DEQUE.poll();
        if (param != null) {
            int hashCode = param.hashCode();
            WAIT.remove(hashCode);
            IMPORTING.put(hashCode, param);
            PROGRESS.put(hashCode, 0D);
        }
        return param;
    }

    public void finish(AsyncImportParam param) {
        int hashCode = param.hashCode();
        IMPORTING.remove(hashCode);
        FINISHED.put(hashCode, param);
        PROGRESS.put(hashCode, 100D);
    }

    /**
     * 更新进度信息
     */
    public void updateProgressBarInfo(Integer hashCode, Integer sheetNum, Double progress) {
        if (hashCode == null) {
            return;
        }
        ImportProgressInfo progressInfo = SHEET_PROGRESS.get(hashCode);
        progressInfo.getProgressMap().put(sheetNum, progress);
        PROGRESS.put(hashCode, ((double)sheetNum / progressInfo.getSheetNums() * 100) + progress / progressInfo.getSheetNums());
        if (progress == 100) {
            logger.info("文件ID: {}, sheetNo: {} 导入完成", hashCode, sheetNum);
        }
        logger.info("文件ID: {}, 导入进度: {}%", hashCode, PROGRESS.get(hashCode));
    }

    public static class Holder {
        private static final Integer SLEEP = 1;

        public Holder() {
            ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
            scheduler.scheduleWithFixedDelay(() -> {
                if (semaphore.tryAcquire()) {
                    try {
                        AsyncImportParam param = INSTANCE.poll();
                        if (param != null) {
                            try {
                                param.getFunction().execute(param);
                                INSTANCE.finish(param);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } finally {
                        semaphore.release();
                    }

                }
            }, 0, SLEEP, TimeUnit.SECONDS);
        }
    }
}
