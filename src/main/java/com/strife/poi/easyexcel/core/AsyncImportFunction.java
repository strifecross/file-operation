package com.strife.poi.easyexcel.core;

import java.io.IOException;

/**
 * @author 孟正浩
 * @date 2024-05-10
 **/
@FunctionalInterface
public interface AsyncImportFunction {

    /**
     * 执行异步导入
     *
     * @param param 异步导入参数
     */
    void execute(AsyncImportParam param) throws IOException;
}
