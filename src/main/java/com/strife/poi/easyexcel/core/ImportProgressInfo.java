package com.strife.poi.easyexcel.core;

import lombok.Data;

import java.util.Map;

/**
 * @author 孟正浩
 * @date 2024-05-10
 **/
@Data
public class ImportProgressInfo {

    private Integer sheetNums;
    private Map<Integer, Double> progressMap;
}
