package com.strife.poi.easyexcel.controller;

import com.strife.file.vo.Response;
import com.strife.poi.easyexcel.service.IExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author mengzhenghao
 * @date 2022/7/17
 */
@RestController
@RequestMapping("/easyexcel")
public class EasyExcelController {

    @Autowired
    private IExcelService easyExcelService;

    /**
     * 简单读取Excel文件
     */
    @PostMapping("/simple-read")
    public Response readExcel(@RequestPart MultipartFile file) throws IOException {
        easyExcelService.simpleReadExcel(file);
        return Response.success();
    }

    /**
     * 异步导入
     */
    @PostMapping("/asyncImport")
    public Response asyncImport(@RequestPart MultipartFile file) throws IOException {
        return Response.success(easyExcelService.asyncImport(file));
    }
}
