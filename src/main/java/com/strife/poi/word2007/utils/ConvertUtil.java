package com.strife.poi.word2007.utils;

import com.strife.poi.word2007.entity.Person;
import org.apache.commons.lang3.time.FastDateFormat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mengzhenghao
 * @date 2022/8/7
 */
public class ConvertUtil {

    public static final FastDateFormat FORMAT = FastDateFormat.getInstance("yyyy.MM.dd");

    /**
     * 将word中的表格数据转换为对象
     *
     * @param data     word提取的表格数据
     * @param hasTitle 表格是否具有标题行
     */
    public static List<Person> convertToPerson(List<List<String>> data, boolean hasTitle) {
        if (hasTitle) {
            data = data.subList(1, data.size());
        }
        return convertToPerson(data, Person.class);
    }

    private static <T> List<T> convertToPerson(List<List<String>> data, Class<T> clazz) {
        if (data == null || data.isEmpty()
                || data.get(0) == null || data.get(0).isEmpty()) {
            return new ArrayList<>();
        }
        Field[] fields = clazz.getDeclaredFields();
        if (fields.length == 0) {
            return new ArrayList<>();
        }
        List<Class<?>> types = new ArrayList<>(fields.length);
        for (Field field : fields) {
            types.add(field.getType());
        }
        List<T> result = new ArrayList<>();
        T t;

        for (List<String> row : data) {
            try {
                Constructor<T> ctor = clazz.getDeclaredConstructor();
                t = ctor.newInstance();
                for (int i = 0; i < row.size(); i++) {
                    setValue(t, row.get(i), fields[i], types.get(i));
                }
                result.add(t);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private static void setValue(Object object, String value, Field field, Class<?> fieldClass) {
        field.setAccessible(true);

        try {
            if (Integer.class.equals(fieldClass) || int.class.equals(fieldClass)) {
                field.set(object, Integer.parseInt(value));
            } else if (Double.class.equals(fieldClass) || double.class.equals(fieldClass)) {
                field.set(object, Double.valueOf(value));
            } else if (Long.class.equals(fieldClass) || long.class.equals(fieldClass)) {
                field.set(object, Long.valueOf(value));
            } else if (Boolean.class.equals(fieldClass) || boolean.class.equals(fieldClass)) {
                field.set(object, Long.valueOf(value));
            } else if (Date.class.equals(fieldClass)) {
                field.set(object, changeStrToData(value));
            } else {
                field.set(object, value);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    private static Date changeStrToData(String value) {
        try {
            return FORMAT.parse(value);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new RuntimeException("类型转换失败");
        }
    }
}
