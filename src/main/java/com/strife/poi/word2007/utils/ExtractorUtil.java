package com.strife.poi.word2007.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 提取word文本 (支持.docx)
 *
 * @author mengzhenghao
 * @date 2022/8/7
 */
public class ExtractorUtil {

    /**
     * 提取文档中的所有文本
     */
    public static String extractorAllCharacter(XWPFDocument document) {
        return new XWPFWordExtractor(document).getText();
    }

    /**
     * 提取word段落(纯文本)
     * 不提取表格中的文字
     */
    public static List<String> extractorParagraph(XWPFDocument document) {
        List<String> result = new ArrayList<>();
        List<XWPFParagraph> paragraphs = document.getParagraphs();
        paragraphs.stream()
                .map(XWPFParagraph::getParagraphText)
                .filter(StringUtils::isNotEmpty)
                .map(text -> text.trim().replaceAll("[\\pZ]", ""))
                .forEach(result::add);
        return result;
    }

    /**
     * 提取页眉文本内容
     */
    public static List<String> extractorHeadersText(XWPFDocument document) {
        List<String> headers = new ArrayList<>();
        List<XWPFHeader> headerList = document.getHeaderList();
        headerList.stream()
                .map(XWPFHeader::getText)
                .forEach(headers::add);
        return headers;
    }

    /**
     * 提取页脚文本内容
     * todo 获取不到页码
     */
    public static List<String> extractorFootersText(XWPFDocument document) {
        List<String> footers = new ArrayList<>();
        List<XWPFFooter> footerList = document.getFooterList();
        footerList.stream()
                .map(XWPFFooter::getText)
                .forEach(footers::add);
        return footers;
    }

    /**
     * 获取表格对象
     */
    public static List<XWPFTable> extractorTable(XWPFDocument document) {
        return document.getTables();
    }

    /**
     * 获取表格数据
     */
    public static List<List<String>> extractorTableData(XWPFDocument document) {
        List<List<String>> result = new ArrayList<>();
        List<String> list;
        List<XWPFTable> tables = document.getTables();
        for (XWPFTable table : tables) {
            for (XWPFTableRow row : table.getRows()) {
                list = new ArrayList<>();
                for (XWPFTableCell cell : row.getTableCells()) {
                    list.add(cell.getText());
                }
                result.add(new ArrayList<>(list));
            }
        }
        return result;
    }

    /**
     * 获取图片对象
     */
    public static List<XWPFPictureData> extractorPictures(XWPFDocument document) {
        return document.getAllPictures();
    }
}
