package com.strife.poi.word2007.entity;

import com.strife.poi.word2007.utils.ConvertUtil;

import java.util.Date;

/**
 * @author mengzhenghao
 * @date 2022/8/7
 */
public class Person {

    private Integer id;

    private String name;

    private String sex;

    private Integer age;

    private Date birth;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", birth=" + ConvertUtil.FORMAT.format(birth) +
                '}';
    }
}
