package com.strife.file.exception;

import com.strife.file.vo.Response;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

/**
 * @author mengzhenghao
 * @date 2022/7/16
 */
@RestControllerAdvice
public class ResponseAdvice {


    @ExceptionHandler(value = IOException.class)
    public Response handleException(IOException e) {
        Response fail = Response.fail();
        fail.setMsg(e.getMessage());
        return fail;
    }

    @ExceptionHandler(value = Exception.class)
    public Response handleException(Exception e) {
        Response fail = Response.fail();
        fail.setMsg(e.getMessage());
        return fail;
    }

    @ExceptionHandler(value = Throwable.class)
    public Response handleException(Throwable e) {
        Response fail = Response.fail();
        fail.setMsg(e.getMessage());
        return fail;
    }
}
