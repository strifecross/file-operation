package com.strife.file.fdfs.controller;

import com.strife.file.common.IFileService;
import com.strife.file.vo.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * FastDfs文件操作
 *
 * @author mengzhenghao
 * @date 2022/7/16
 */
@RequestMapping("/fastdfs")
@RestController
public class FastDfsController {

    @Autowired
    private IFileService fastDfsService;


    /**
     * 文件上传
     */
    @PostMapping("/upload")
    public Response upload(MultipartFile file) throws Exception {
        return Response.success(fastDfsService.upload(file));
    }

    /**
     * 文件下载
     */
    @GetMapping("/download")
    public void download(@RequestParam String fileName,
                         HttpServletRequest request,
                         HttpServletResponse response) throws Exception {
        fastDfsService.download(fileName, request, response);
    }

    /**
     * 文件删除
     */
    @DeleteMapping("/delete")
    public Response delete(@RequestParam String fileName) throws Exception {
        fastDfsService.delete(fileName);
        return Response.success();
    }
}
