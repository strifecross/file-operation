package com.strife.file.fdfs.service;

import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.domain.proto.storage.DownloadFileStream;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.github.tobato.fastdfs.service.TrackerClient;
import com.strife.file.common.IFileService;
import com.strife.file.local.utils.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * @author mengzhenghao
 * @date 2022/7/16
 */
@Service
public class FastDfsService implements IFileService {

    private static final Logger logger = LoggerFactory.getLogger(FastDfsService.class);

    @Value("${fdfs.domain}")
    private String domain;

    @Autowired
    private FastFileStorageClient storageClient;

    @Autowired
    private TrackerClient trackerClient;

    @Override
    public String upload(MultipartFile file) throws IOException {
        logger.debug("fastdfs文件上传开始");
        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
        } catch (IOException e) {
            logger.error("无法读取文件，文件名称：{}，异常原因：{}", file.getOriginalFilename(), e.getMessage());
            throw e;
        }
        StorePath storePath = storageClient.uploadFile(getGroupName(), inputStream, file.getSize(),
                FilenameUtils.getExtension(file.getOriginalFilename()));
        logger.debug("fastdfs文件上传完成");
        return domain + "/" + storePath.getFullPath();
    }

    @Override
    public void download(String fileName, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.debug("fastdfs文件下载开始");
        ServletOutputStream outputStream = null;

        try {
            outputStream = response.getOutputStream();
            storageClient.downloadFile(getGroupName(), fileName, new DownloadFileStream(outputStream));

            response.setCharacterEncoding(StandardCharsets.UTF_8.name());
            response.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, fileName));
        } catch (Exception e) {
            logger.error("下载失败，原因：{}", e.getMessage());
            response.setHeader("err-msg", FileUtils.setFileDownloadHeader(request, e.getMessage()));
            throw e;
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        logger.debug("fastdfs文件下载完成");
    }

    @Override
    public void delete(String fileName) {
        logger.debug("文件删除开始");
        try {
            storageClient.deleteFile(getGroupName(), fileName);
        } catch (Exception e) {
            logger.error("文件删除失败：{}", e.getMessage());
            throw e;
        }
        logger.debug("文件删除完成");
    }

    private String getGroupName() {
        return trackerClient.getStoreStorage().getGroupName();
    }
}
