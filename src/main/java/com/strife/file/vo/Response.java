package com.strife.file.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * @author mengzhenghao
 * @date 2022/7/16
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response implements Serializable {

    private int code;

    private String msg;

    private Object data;

    public Response() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static Response success() {
        Response response = new Response();
        response.setCode(200);
        response.setMsg("操作成功");
        return response;
    }

    public static Response success(Object data) {
        Response response = success();
        response.setData(data);
        return response;
    }

    public static Response fail() {
        Response response = new Response();
        response.setCode(500);
        response.setMsg("操作失败");
        return response;
    }

    public static Response fail(String msg) {
        Response response = fail();
        response.setMsg(msg);
        return response;
    }

    public static Response fail(int code, String msg) {
        Response response = new Response();
        response.setCode(code);
        response.setMsg(msg);
        return response;
    }
}
