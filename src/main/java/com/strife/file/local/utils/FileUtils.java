package com.strife.file.local.utils;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.UUID;

/**
 * 文件工具类
 *
 * @author mengzhenghao
 * @date 2022/7/10
 */
public class FileUtils {

    /**
     * 字符常量：斜杠 {@code '/'}
     */
    public static final char SLASH = '/';
    /**
     * 字符常量：反斜杠 {@code '\\'}
     */
    public static final char BACKSLASH = '\\';
    /**
     * 字符串常量：横杆 {@code '-'}
     */
    public static final String LINE = "-";
    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);
    /**
     * 默认文件大小限制 50MB
     * TODO 做成可配置
     */
    private static final int DEFAULT_MAX_SIZE = 50 * 1024 * 1024;

    /**
     * 默认文件名最大长度 100
     * TODO 做成可配置
     */
    private static final int DEFAULT_FILE_NAME_LENGTH = 100;

    private FileUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 本地文件上传
     *
     * @param baseDir 相对应用的基目录
     * @param file    文件
     * @return 返回上传后的本地存储的文件名称
     */
    public static String upload(String baseDir, MultipartFile file) throws IOException {
        try {
            return upload(baseDir, file, MimeTypeUtils.DEFAULT_ALLOWED_EXTENSION);
        } catch (Exception e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /**
     * 文件上传
     *
     * @param baseDir          相对应用的基目录
     * @param file             上传的文件
     * @param allowedExtension 上传文件类型
     * @return 返回上传成功的文件名
     */
    private static String upload(String baseDir, MultipartFile file, String[] allowedExtension) throws IOException {
        String originalFilename = file.getOriginalFilename();
        if (StringUtils.isNotEmpty(originalFilename)) {
            int fileNameLength = originalFilename.length();
            if (fileNameLength > DEFAULT_FILE_NAME_LENGTH) {
                throw new IOException("文件名称太长，不能超过100个字符");
            }
        }

        long fileSize = file.getSize();
        if (fileSize > DEFAULT_MAX_SIZE) {
            throw new IOException("文件太大，不能超过50MB");
        }

        assertAllows(file, allowedExtension);

        String fileName = extractFilename(file);
        File desc = getAbsoluteFile(baseDir, fileName);
        file.transferTo(desc);
        fileName = getPathName(fileName);
        return fileName;
    }

    private static String getPathName(String fileName) {
        return File.separator + fileName;
    }

    private static File getAbsoluteFile(String uploadDir, String fileName) throws IOException {
        File desc = new File(uploadDir + File.separator + fileName);

        if (!desc.exists() && !desc.getParentFile().exists()) {
            boolean flag = desc.getParentFile().mkdirs();
            if (!flag) {
                throw new IOException("文件夹创建失败");
            }
        }
        return desc.isAbsolute() ? desc : desc.getAbsoluteFile();
    }

    private static void assertAllows(MultipartFile file, String[] allowedExtension) throws IOException {
        String extension = getExtension(file);
        if (allowedExtension != null && !isAllowedExtension(extension, allowedExtension)) {
            throw new IOException("该文件类型不允许上传，文件类型：" + extension);
        }
    }

    /**
     * 判断MIME类型是否是允许的MIME类型
     *
     * @param extension        上传文件类型
     * @param allowedExtension 允许上传文件类型
     * @return true/false
     */
    private static boolean isAllowedExtension(String extension, String[] allowedExtension) {
        for (String str : allowedExtension) {
            if (str.equalsIgnoreCase(extension)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取文件后缀名
     */
    public static String getExtension(MultipartFile file) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (StringUtils.isEmpty(extension)) {
            extension = MimeTypeUtils.getExtension(Objects.requireNonNull(file.getContentType()));
        }
        return extension;
    }

    /**
     * 编码文件名
     */
    public static String extractFilename(MultipartFile file) {
        String extension = getExtension(file);
        String uuid = UUID.randomUUID().toString().replace("-", "");
        return uuid + "." + extension;
    }

    /**
     * 下载文件名重新编码
     *
     * @param request  请求对象
     * @param fileName 文件名
     * @return 编码后的文件名
     */
    public static String setFileDownloadHeader(HttpServletRequest request, String fileName) {
        final String agent = request.getHeader("USER-AGENT");
        // TODO 将文件名改为上传时的文件名，连数据库
        String filename = fileName;
        if (agent.contains("MSIE")) {
            // IE浏览器
            filename = URLEncoder.encode(filename, StandardCharsets.UTF_8);
            filename = filename.replace("+", " ");
        } else if (agent.contains("Firefox")) {
            // 火狐浏览器
            filename = new String(fileName.getBytes(), StandardCharsets.ISO_8859_1);
        } else if (agent.contains("Chrome")) {
            // google浏览器
            filename = URLEncoder.encode(filename, StandardCharsets.UTF_8);
        } else {
            // 其它浏览器
            filename = URLEncoder.encode(filename, StandardCharsets.UTF_8);
        }
        return filename;
    }

    /**
     * 输出指定文件的byte数组
     *
     * @param filePath 文件路径
     * @param os       输出流
     * @return
     */
    public static void writeBytes(String filePath, OutputStream os) throws IOException {
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                throw new FileNotFoundException(filePath);
            }
            try (FileInputStream fis = new FileInputStream(file)) {
                byte[] bytes = new byte[1024];
                int length = 0;
                while ((length = fis.read(bytes)) > 0) {
                    os.write(bytes, 0, length);
                }
            }
        } catch (FileNotFoundException e) {
            throw new IOException("文件不存在 -> " + filePath);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 判断文件是否为图片
     */
    public static boolean isImage(MultipartFile file) {
        String extension = getExtension(file);
        for (String type : MimeTypeUtils.IMAGE_EXTENSION) {
            if (extension.equalsIgnoreCase(type)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断文件名称是否为符合图片格式
     */
    public static boolean isImage(String fileName) {
        String extension = FilenameUtils.getExtension(fileName);
        for (String type : MimeTypeUtils.IMAGE_EXTENSION) {
            if (extension.equalsIgnoreCase(type)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 分块上传文件
     *
     * @param baseDir  基路径
     * @param fileName 文件名
     * @param file     分块文件
     * @throws IOException exception
     */
    public static void uploadBlock(String baseDir, String fileName, MultipartFile file) throws IOException {
        File dest = getAbsoluteFile(baseDir, fileName);
        file.transferTo(dest);
    }

    /**
     * 删除文件或文件夹
     *
     * @param fileName 文件名
     * @return 删除成功返回true, 失败返回false
     */
    public static boolean deleteFileOrDirectory(String fileName) {
        // fileName是路径或者file.getPath()获取的文件路径
        File file = new File(fileName);
        if (file.exists()) {
            if (file.isFile()) {
                // 是文件，调用删除文件的方法
                return deleteFile(fileName);
            } else {
                // 是文件夹，调用删除文件夹的方法
                return deleteDirectory(fileName);
            }
        } else {
            logger.error("文件或文件夹删除失败：{}", fileName);
            return false;
        }
    }

    /**
     * 删除文件
     *
     * @param fileName 文件名
     * @return 删除成功返回true, 失败返回false
     */
    private static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        return file.delete();
    }

    /**
     * 删除文件夹
     * 删除文件夹需要把包含的文件及文件夹先删除，才能成功
     *
     * @param directory 文件夹名
     * @return 删除成功返回true, 失败返回false
     */
    private static boolean deleteDirectory(String directory) {
        // directory不以文件分隔符（/或\）结尾时，自动添加文件分隔符，不同系统下File.separator方法会自动添加相应的分隔符
        if (!directory.endsWith(File.separator)) {
            directory = directory + File.separator;
        }
        File directoryFile = new File(directory);
        // 判断directory对应的文件是否存在，或者是否是一个文件夹
        if (!directoryFile.exists() || !directoryFile.isDirectory()) {
            logger.error("文件夹删除失败，文件夹不存在: {}", directory);
            return false;
        }
        boolean flag = true;
        // 删除文件夹下的所有文件和文件夹
        File[] files = directoryFile.listFiles();
        // 循环删除所有的子文件及子文件夹
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    // 删除子文件
                    flag = deleteFile(file.getAbsolutePath());
                } else {
                    // 删除子文件夹
                    flag = deleteDirectory(file.getAbsolutePath());
                }
                if (!flag) {
                    break;
                }
            }
        }

        if (!flag) {
            return false;
        }
        // 最后删除当前文件夹
        return directoryFile.delete();
    }

    /**
     * /aaa/bbb/xxx.docx -> xxx
     * D:\aaa\bbb\xxx.txt -> xxx
     * @param fileFullName 文件全路径
     * @return 返回文件名
     */
    public static String getFileName(String fileFullName) {
        if (StringUtils.isBlank(fileFullName)) {
            return "";
        }
        return fileFullName.substring(fileFullName.lastIndexOf(File.separator) + 1,
                fileFullName.lastIndexOf("."));
    }

    /**
     * /aaa/bbb/xxx.docx -> /aaa/bbb/
     * D:\aaa\bbb\xxx.txt -> D:\aaa\bbb\
     * @param fileFullName 文件全路径
     * @return 返回文件路径
     */
    public static String getFilePath(String fileFullName) {
        if (StringUtils.isBlank(fileFullName)) {
            return "";
        }
        return fileFullName.substring(0, fileFullName.lastIndexOf(File.separator) + 1);
    }

}
