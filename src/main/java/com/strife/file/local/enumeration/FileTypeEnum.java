package com.strife.file.local.enumeration;

/**
 * 文件类型枚举
 *
 * @author mengzhenghao
 * @date 2022/7/10
 */
public enum FileTypeEnum {

    /** 其他 */
    OTHER("/other"),

    /** 图片 */
    IMAGE("/img"),

    /** 音频 */
    AUDIO("/audio"),

    /** 视频 */
    VIDEO("/video")
    ;


    private final String path;

    FileTypeEnum(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
