package com.strife.file.local.controller;

import com.strife.file.local.service.LocalFileService;
import com.strife.file.vo.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 本地文件操作
 *
 * @author mengzhenghao
 * @date 2022/7/10
 */
@RestController
@RequestMapping("/local")
public class LocalFileController {

    @Autowired
    private LocalFileService localFileService;

    /**
     * 本地文件上传
     *
     * @param file     上传的文件
     * @param fileType 文件类型
     */
    @PostMapping("/upload")
    public Response uploadFile(MultipartFile file,
                               @RequestParam(required = false) Long fileType) throws IOException {
        return Response.success(localFileService.upload(file, fileType));
    }

    /**
     * 本地文件下载
     */
    @PostMapping("/download")
    public void downloadFile(@RequestParam Long fileType,
                             @RequestParam String fileName,
                             HttpServletRequest request,
                             HttpServletResponse response) throws Exception {

        localFileService.download(fileType, fileName, request, response);
    }

    /**
     * 本地文件删除
     */
    @DeleteMapping("/delete")
    public Response deleteFile(@RequestParam String fileName,
                               @RequestParam(required = false) Long type) throws Exception {
        localFileService.delete(fileName, type);
        return Response.success();
    }

    /**
     * 查看已上传文件列表
     */
    @GetMapping("/list")
    public Response select() {
        return Response.success(localFileService.select());
    }
}
