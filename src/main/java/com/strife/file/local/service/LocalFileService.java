package com.strife.file.local.service;

import com.strife.file.local.controller.LocalFileController;
import com.strife.file.local.enumeration.FileTypeEnum;
import com.strife.file.common.IFileService;
import com.strife.file.local.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mengzhenghao
 * @date 2022/7/10
 */
@Service
public class LocalFileService implements IFileService {

    private static final Logger logger = LoggerFactory.getLogger(LocalFileController.class);

    /**
     * 资源映射路径 前缀
     */
    @Value("${file.prefix}")
    public String localFilePrefix;

    /**
     * 域名或本机访问地址
     */
    @Value("${file.domain}")
    public String domain;

    /**
     * 上传文件存储在本地的根路径
     */
    @Value("${file.path}")
    private String localFilePath;

    @Override
    public String upload(MultipartFile file, Long type) throws IOException {
        logger.debug("本地文件上传开始");
        String path = getPath(type);
        String name;
        try {
            name = FileUtils.upload(localFilePath + path, file);
        } catch (IOException e) {
            logger.error("文件上传出错，原因：{}", e.getMessage());
            throw e;
        }
        logger.debug("本地文件上传完成");
        return domain + localFilePrefix + path + name;
    }

    @Override
    public void download(Long type, String resource, HttpServletRequest request, HttpServletResponse response) throws IOException {
        logger.debug("本地文件下载开始");
        // 本地资源路径
        String localPath = localFilePath + getPath(type) + File.separator;
        // 资源地址
        String downloadPath = localPath + resource;
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, resource));
        try {
            FileUtils.writeBytes(downloadPath, response.getOutputStream());
        } catch (IOException e) {
            logger.error("本地文件下载异常，异常原因：{}", e.getMessage());
            throw e;
        }
        logger.debug("本地文件下载完成");
    }

    @Override
    public void delete(String fileName, Long type) throws Exception {
        logger.debug("本地文件删除开始");
        // 本地资源路径
        String localPath = localFilePath + getPath(type) + File.separator;
        // 资源地址
        String filePath = localPath + fileName;
        File file = new File(filePath);
        if (!file.exists()) {
            logger.error("文件不存在");
            throw new Exception("文件已删除或不存在，文件名：" + fileName);
        }
        try{
            boolean flag = file.delete();
            if (flag) {
                logger.debug("文件删除成功");
            } else {
                logger.debug("文件删除失败");
            }
        } catch (Exception e) {
            logger.error("文件删除失败，原因：{}", e.getMessage());
        }
    }

    @Override
    public List<String> select() {
        List<String> res = new ArrayList<>();
        for (FileTypeEnum typeEnum : FileTypeEnum.values()) {
            String path = localFilePath + typeEnum.getPath();
            File directory = new File(path);
            if (directory.exists() && directory.isDirectory()) {
                File[] files = directory.listFiles();
                if (files != null && files.length > 0) {
                    for (File file : files) {
                        res.add(typeEnum.getPath() + File.separator + file.getName());
                    }
                }
            }
        }
        return res;
    }

    /**
     * 根据文件类型获取文件上传相对路径
     */
    private static String getPath(Long type) {
        int tyeToInt = type.intValue();
        for (FileTypeEnum typeEnum : FileTypeEnum.values()) {
            if (tyeToInt == typeEnum.ordinal()) {
                return typeEnum.getPath();
            }
        }
        return FileTypeEnum.OTHER.getPath();
    }
}
