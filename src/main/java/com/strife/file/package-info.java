/**
 * 文件操作
 * 1、本地文件上传、下载、删除
 * 2、fastdfs文件上传、下载、删除
 * 3、minio文件上传、下载、删除
 * @author mengzhenghao
 * @date 2022/7/17
 */
package com.strife.file;
