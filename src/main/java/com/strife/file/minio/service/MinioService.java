package com.strife.file.minio.service;

import com.strife.file.common.IFileService;
import com.strife.file.local.utils.FileUtils;
import com.strife.file.minio.config.MinioConfig;
import io.minio.*;
import io.minio.messages.Item;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mengzhenghao
 * @date 2022/7/17
 */
@Service
public class MinioService implements IFileService {

    private static final Logger logger = LoggerFactory.getLogger(IFileService.class);

    @Autowired
    private MinioConfig minioConfig;

    @Autowired
    private MinioClient minioClient;

    @Override
    public String upload(MultipartFile file) throws Exception {
        logger.debug("开始文件上传");
        checkBucketExist();

        String filename = FileUtils.extractFilename(file);
        PutObjectArgs args;

        String bucketName = minioConfig.getBucketName();
        if (FileUtils.isImage(file)) {
            bucketName = minioConfig.getBucketImageName();
        }
        try {
            args = PutObjectArgs.builder()
                    .bucket(bucketName)
                    .object(filename)
                    .stream(file.getInputStream(), file.getSize(), -1)
                    .contentType(file.getContentType())
                    .build();
        } catch (IOException e) {
            logger.error("文件上传失败，原因：{}", e.getMessage());
            throw e;
        }
        try {
            minioClient.putObject(args);
        } catch (Exception e) {
            logger.error("文件上传失败，原因：{}", e.getMessage());
            throw e;
        }


        logger.debug("文件上传完成");
        return minioConfig.getUrl() + File.separator + bucketName + File.separator + filename;
    }

    /**
     * 检查bucket是否存在，不存在则创建
     */
    private void checkBucketExist() throws Exception {
        try {
            String bucketName = minioConfig.getBucketName();
            boolean bucketExists = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
            if (!bucketExists) {
                logger.debug("不存在bucket：{}，正在创建……", bucketName);
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
                logger.debug("创建bucket成功");
            }
            bucketName = minioConfig.getBucketImageName();
            bucketExists = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
            if (!bucketExists) {
                logger.debug("不存在bucket：{}，正在创建……", bucketName);
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
                logger.debug("创建bucket成功");
            }
        } catch (Exception e) {
            logger.error("检查bucket失败，原因：{}", e.getMessage());
            throw e;
        }

    }

    @Override
    public void download(String fileName, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.debug("开始下载文件");

        String bucketName = minioConfig.getBucketName();
        if (FileUtils.isImage(fileName)) {
            bucketName = minioConfig.getBucketImageName();
        }

        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, fileName));

        try (InputStream inputStream = minioClient.getObject(GetObjectArgs.builder()
                .bucket(bucketName)
                .object(fileName)
                .build());
             ServletOutputStream outputStream = response.getOutputStream()) {
            IOUtils.copy(inputStream, outputStream);
            logger.debug("文件下载完成");
        } catch (Exception e) {
            logger.error("文件下载失败， 原因：{}", e.getMessage());
            throw e;
        }

    }

    @Override
    public void delete(String fileName) throws Exception {
        logger.debug("开始删除文件");
        String bucketName = minioConfig.getBucketName();
        if (FileUtils.isImage(fileName)) {
            bucketName = minioConfig.getBucketImageName();
        }
        try {
            minioClient.removeObject(RemoveObjectArgs.builder()
                    .bucket(bucketName)
                    .object(fileName)
                    .build());
            logger.debug("文件删除成功，文件名：{}", fileName);
        } catch (Exception e) {
            logger.error("文件删除失败，原因：{}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<String> select() throws Exception {
        logger.debug("开始查询所有文件");
        List<String> res = new ArrayList<>();
        Iterable<Result<Item>> iterableImage = minioClient.listObjects(ListObjectsArgs.builder()
                .bucket(minioConfig.getBucketImageName())
                .build());
        Iterable<Result<Item>> iterableNonImage = minioClient.listObjects(ListObjectsArgs.builder()
                .bucket(minioConfig.getBucketName())
                .build());
        logger.debug("成功从Minio获取到Result<Item>");
        for (Result<Item> item : iterableImage) {
            try {
                res.add(item.get().objectName());
            } catch (Exception e) {
                logger.error("对象解析失败，原因：{}", e.getMessage());
                throw e;
            }
        }
        for (Result<Item> item : iterableNonImage) {
            try {
                res.add(item.get().objectName());
            } catch (Exception e) {
                logger.error("对象解析失败，原因：{}", e.getMessage());
                throw e;
            }
        }
        logger.debug("查询所有文件完成");
        return res;
    }
}
