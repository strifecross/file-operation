package com.strife.file.minio.controller;

import com.strife.file.common.IFileService;
import com.strife.file.vo.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author mengzhenghao
 * @date 2022/7/17
 */
@RestController
@RequestMapping("/minio")
public class MinioController {

    @Autowired
    private IFileService minioService;

    /**
     * 文件上传
     */
    @PostMapping("/upload")
    public Response upload(MultipartFile file) throws Exception {
        return Response.success(minioService.upload(file));
    }

    /**
     * 文件下载
     */
    @GetMapping("/download")
    public void download(@RequestParam String fileName,
                         HttpServletRequest request,
                         HttpServletResponse response) throws Exception {
        minioService.download(fileName, request, response);
    }

    /**
     * 文件删除
     */
    @DeleteMapping("/delete")
    public Response delete(@RequestParam String fileName) throws Exception {
        minioService.delete(fileName);
        return Response.success();
    }

    /**
     * 查询所有已上传文件
     */
    @GetMapping("/select")
    public Response select() throws Exception {
        return Response.success(minioService.select());
    }
}
