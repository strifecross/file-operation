package com.strife.file.common;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件操作接口
 *
 * @author mengzhenghao
 * @date 2022/7/10
 */
public interface IFileService {

    /**
     * 上传文件，返回上传地址
     *
     * @param file 文件
     * @return 返回上传地址
     */
    default String upload(MultipartFile file) throws Exception {
        throw new IllegalStateException("无具体实现");
    }

    /**
     * 上传文件，返回上传地址
     *
     * @param file 文件
     * @param type 文件类型
     * @return 返回上传地址
     */
    default String upload(MultipartFile file, Long type) throws Exception {
        throw new IllegalStateException("无具体实现");
    }

    /**
     * 下载文件
     *
     * @param fileName 文件名称
     * @param response 响应
     */
    default void download(String fileName, HttpServletRequest request, HttpServletResponse response) throws Exception {
        throw new IllegalStateException("无具体实现");
    }

    /**
     * 下载文件
     *
     * @param type     文件类型
     * @param fileName 文件名称
     * @param response 响应
     */
    default void download(Long type, String fileName, HttpServletRequest request, HttpServletResponse response) throws Exception {
        throw new IllegalStateException("无具体实现");
    }

    /**
     * 删除文件
     *
     * @param fileName 文件名
     */
    default void delete(String fileName) throws Exception {
        throw new IllegalStateException("无具体实现");
    }

    /**
     * 删除文件
     *
     * @param fileName 文件名
     * @param type     文件类型
     */
    default void delete(String fileName, Long type) throws Exception {
        throw new IllegalStateException("无具体实现");
    }

    /**
     * 查询已上传文件列表
     *
     * @return 返回文件列表数据
     */
    default List<String> select() throws Exception {
        return new ArrayList<>();
    }
}
