package com.strife.file.bigfile.service;

import com.strife.file.bigfile.util.BlockFileUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author: strife
 * @date: 2022/10/4
 */
@Service
public class BigFileService {


    /**
     * 上传文件存储在本地的根路径
     */
    @Value("${file.path}")
    private String localFilePath;

    /**
     * 分块上传文件
     *
     * @param md5    文件整体内容MD5签名值
     * @param chunks 分块总数
     * @param chunk  当前分块是第几个分块
     * @param file   分块文件
     */
    public void uploadWithBlock(String md5, Integer chunks, Integer chunk, MultipartFile file) throws IOException {
        // 获取文件的文件名
        String fileName = BlockFileUtil.getFileName(md5, chunks, file);
        // 上传分块
        BlockFileUtil.upload(localFilePath, md5, fileName, file, chunk);
        // 添加分块上传记录
        BlockFileUtil.addChunk(md5, chunk);
        // 判断分块是否全部上传完毕
        if (BlockFileUtil.uploadDone(md5)) {
            BlockFileUtil.removeKey(md5);
            // 合并分块
            BlockFileUtil.mergeBlock(localFilePath, fileName, md5);
            // 删除分块记录
            BlockFileUtil.deleteBlock(localFilePath, md5);
        }
    }
}
