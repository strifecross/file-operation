package com.strife.file.bigfile.util;

import com.strife.file.local.enumeration.FileTypeEnum;
import com.strife.file.local.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 分块文件上传工具类
 *
 * @author: strife
 * @date: 2022/10/4
 */
public class BlockFileUtil {

    private static final Logger logger = LoggerFactory.getLogger(BlockFileUtil.class);

    private static final Map<String, Value> CHUNK_MAP = new ConcurrentHashMap<>();

    private BlockFileUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 获取随机生成的文件名
     *
     * @param key    文件内容的MD5签名
     * @param chunks 文件分块总数
     * @return 返回文件的文件名
     */
    public static String getFileName(String key, int chunks, MultipartFile file) {
        if (!isExist(key)) {
            synchronized (BlockFileUtil.class) {
                if (!isExist(key)) {
                    CHUNK_MAP.put(key, new Value(chunks, file));
                }
            }
        }
        return CHUNK_MAP.get(key).name;
    }

    /**
     * 判断文件是否有分块已上传
     *
     * @param key
     * @return
     */
    private static boolean isExist(String key) {
        return CHUNK_MAP.containsKey(key);
    }

    /**
     * 获取文件上传的最终文件夹
     */
    private static String getFinalDir(String baseDir, String md5) {
        return baseDir + File.separator + md5;
    }

    /**
     * 获取分块文件上传的最终文件名
     */
    private static String getFinalBlockName(String fileName, Integer chunk) {
        return chunk + FileUtils.LINE + fileName;
    }

    /**
     * 分块上传文件
     *
     * @param baseDir  基路径
     * @param md5      文件内容MD5
     * @param fileName 文件名
     * @param file     分块文件
     * @param chunk    当前分块是第几个分块
     * @throws IOException exception
     */
    public static void upload(String baseDir, String md5, String fileName, MultipartFile file, Integer chunk) throws IOException {
        FileUtils.uploadBlock(getFinalDir(baseDir, md5), getFinalBlockName(fileName, chunk), file);
    }

    /**
     * 合并分块
     *
     * @param baseDir  基路径
     * @param fileName 文件名
     * @param md5      文件内容MD5签名
     */
    public static void mergeBlock(String baseDir, String fileName, String md5) throws IOException {
        logger.debug("merge block start");
        String finalDir = getFinalDir(baseDir, md5);
        File dir = new File(finalDir);
        if (!dir.exists()) {
            throw new IOException("directory is not exist");
        }

        if (!dir.isDirectory()) {
            throw new IOException(finalDir + "is not a directory");
        }

        // 获取所有分块文件
        File[] blockFiles = dir.listFiles();
        if (blockFiles == null) {
            throw new RuntimeException("there is no blockFiles");
        }
        // 按照分块顺序排序
        List<File> blockFilesList = Arrays.stream(blockFiles)
                .sorted(Comparator.comparing(File::getName))
                .collect(Collectors.toList());
        File dest = new File(baseDir + FileTypeEnum.OTHER.getPath() + File.separator + fileName);
        if (!dest.exists() && !dest.getParentFile().exists()) {
            boolean flag = dest.getParentFile().mkdirs();
            if (!flag) {
                throw new IOException("文件夹创建失败");
            }
        }
        try (
                FileOutputStream outputStream = new FileOutputStream(dest)
        ) {
            byte[] data = new byte[1024];
            for (File blockFile : blockFilesList) {
                try (FileInputStream inputStream = new FileInputStream(blockFile)) {
                    int len;
                    while ((len = inputStream.read(data)) != -1) {
                        outputStream.write(data, 0, len);
                    }
                }
            }
        }
        logger.debug("merge block end");
    }

    /**
     * 为文件添加上传分块记录
     *
     * @param key
     * @param chunk
     */
    public static void addChunk(String key, int chunk) {
        CHUNK_MAP.get(key).status[chunk] = true;
    }

    /**
     * 判断文件所有分块是否已上传
     *
     * @param key
     * @return
     */
    public static boolean uploadDone(String key) {
        if (isExist(key)) {
            for (boolean b : CHUNK_MAP.get(key).status) {
                if (!b) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * 从map中删除键为key的键值对
     *
     * @param key
     */
    public static void removeKey(String key) {
        if (isExist(key)) {
            CHUNK_MAP.remove(key);
        }
    }

    /**
     * 删除分块文件
     *
     * @param baseDir 基路径
     * @param md5     文件内容MD5签名
     */
    public static void deleteBlock(String baseDir, String md5) {
        String finalDir = getFinalDir(baseDir, md5);
        FileUtils.deleteFileOrDirectory(finalDir);
    }

    /**
     * 内部类记录分块上传文件信息
     */
    private static class Value {
        String name;
        boolean[] status;

        Value(int n, MultipartFile file) {
            this.name = FileUtils.extractFilename(file);
            this.status = new boolean[n];
        }
    }
}
