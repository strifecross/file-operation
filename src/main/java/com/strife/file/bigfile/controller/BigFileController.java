package com.strife.file.bigfile.controller;

import com.strife.file.bigfile.service.BigFileService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 大文件上传(分块上传)
 *
 * @author: strife
 * @date: 2022/10/4
 */
@CrossOrigin
@RestController
@RequestMapping("/big")
public class BigFileController {

    @Resource
    private BigFileService bigFileService;

    /**
     * 分块文件上传
     *
     * @param md5 对文件内容进行md5签名
     * @param chunks 文件分块总数
     * @param chunk 当前分块是第几块
     * @param file 当前分块文件
     * @throws IOException exception
     */
    @PostMapping("/upload")
    public void upload(String md5,
                       Integer chunks,
                       Integer chunk,
                       MultipartFile file) throws IOException {
        bigFileService.uploadWithBlock(md5, chunks, chunk, file);
    }
}