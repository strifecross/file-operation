package com.strife.export.word.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import com.strife.export.word.entity.TemplateVar;
import com.strife.export.word.util.WordUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author: strife
 * @date: 2023/9/16
 */
public class WordService implements IWordService{

    private static final Logger logger = LoggerFactory.getLogger(WordService.class);


    @Override
    public void renderTemplate(String templatePath, String outputPath, List<TemplateVar> templateVars) {
        Map<String, Object> data = new HashMap<>();
        if (CollectionUtil.isNotEmpty(templateVars)) {
            data = templateVars
                    .stream()
                    .collect(Collectors.toMap(TemplateVar::getKey, TemplateVar::getValue, (a, b) -> a));
        }
        try {
            WordUtil.render(templatePath, outputPath, data);
        } catch (IOException e) {
            logger.error("渲染word模板失败", e);
            throw new RuntimeException(e);
        }
    }
}
