package com.strife.export.word.service;

import com.strife.export.word.entity.TemplateVar;

import java.util.List;

/**
 * @author: strife
 * @date: 2023/9/16
 */
public interface IWordService {

    /**
     * 渲染Word模板
     *
     * @param templatePath 模板路径
     * @param outputPath   输出路径
     * @param templateVars 变量
     */
    void renderTemplate(String templatePath, String outputPath, List<TemplateVar> templateVars);

}
