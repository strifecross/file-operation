package com.strife.export.word.util;

import com.deepoove.poi.XWPFTemplate;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

/**
 * @author: strife
 * @date: 2023/9/16
 */
public class WordUtil {


    /**
     * 使用poi-lt渲染Word模板
     *
     * @param templatePath 模板路径
     * @param outputPath   输出Word文件路径
     * @param data         数据
     * @throws IOException e
     */
    public static void render(String templatePath, String outputPath, Map<String, Object> data) throws IOException {
        XWPFTemplate template = XWPFTemplate.compile(templatePath).render(data);
        template.writeAndClose(new FileOutputStream(outputPath));
    }
}
