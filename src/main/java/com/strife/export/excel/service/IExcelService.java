package com.strife.export.excel.service;

import com.strife.export.excel.util.ExcelUtil;
import org.apache.poi.ss.usermodel.CellStyle;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @description Excel接口
 * @author: strife
 * @date: 2022/8/14
 */
public interface IExcelService {

    /**
     * 默认样式
     */
    String STYLE_DEFAULT = "default";
    /**
     * 特殊样式
     */
    String STYLE_SPECIAL = "special";


    /**
     * 默认单元格样式
     *
     * @return {@link CellStyle}
     */
    CellStyle defaultStyle();

    /**
     * 标题行样式
     *
     * @return {@link CellStyle}
     */
    CellStyle getTitleStyle();

    /**
     * 特定列样式
     * 默认为字体为红色宋体，需要变更时可自行实现，传入具体实现IExcelService类即可
     *
     * @return {@link CellStyle}
     * @see ExcelUtil#exportExcel(List, Class, Class, HttpServletResponse, String)
     */
    default CellStyle specialColumnStyle() {
        return null;
    }
}
