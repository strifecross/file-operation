package com.strife.export.excel.service.impl;

import com.strife.export.excel.service.IExcelService;
import org.apache.poi.ss.usermodel.*;

/**
 * @description Excel接口实现类
 * @author: strife
 * @date: 2022/8/14
 */
public class ExcelServiceImpl implements IExcelService {

    private final Workbook workbook;

    public ExcelServiceImpl(Workbook workbook) {
        this.workbook = workbook;
    }

    /**
     * 默认单元格样式
     *
     * @return {@link CellStyle}
     */
    @Override
    public CellStyle defaultStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("宋体");
        cellStyle.setFont(font);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        setAroundBorder(cellStyle, BorderStyle.THIN);
        return cellStyle;
    }

    /**
     * 标题行样式
     *
     * @return {@link CellStyle}
     */
    @Override
    public CellStyle getTitleStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("宋体");
        cellStyle.setFont(font);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        setAroundBorder(cellStyle, BorderStyle.THIN);
        return cellStyle;
    }

    /**
     * 特定列样式
     *
     * @return {@link CellStyle}
     */
    @Override
    public CellStyle specialColumnStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("宋体");
        font.setColor(Font.COLOR_RED);
        cellStyle.setFont(font);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        setAroundBorder(cellStyle, BorderStyle.THIN);
        return cellStyle;
    }

    private void setAroundBorder(CellStyle cellStyle, BorderStyle borderStyle) {
        cellStyle.setBorderBottom(borderStyle);
        cellStyle.setBorderRight(borderStyle);
        cellStyle.setBorderTop(borderStyle);
        cellStyle.setBorderLeft(borderStyle);
    }
}
