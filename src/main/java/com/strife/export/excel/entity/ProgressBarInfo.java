package com.strife.export.excel.entity;

import lombok.Data;

/**
 * @author 孟正浩
 * @date 2024-05-08
 **/
@Data
public class ProgressBarInfo {

    /**
     * id
     */
    private Integer id;
    /**
     * 进度
     */
    private double progressBar;
}
