package com.strife.export.excel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 孟正浩
 * @date 2024-05-30
 **/
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CellData {

    /** sheet页 */
    private Integer sheetNo;
    /** 行号 */
    private Integer rowIndex;
    /** 列号 */
    private Integer colIndex;
    /** 数据 */
    private Object data;
}
