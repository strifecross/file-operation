package com.strife.export.excel.annotation;

import com.strife.export.excel.service.IExcelService;

import java.lang.annotation.*;

/**
 * @description Excel注解
 * @author: strife
 * @date: 2022/8/14
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Excel {

    /**
     * 字段导出对应的列名
     */
    String value();

    /**
     * 导出Excel列宽
     */
    int width() default 2000;

    /**
     * 排序字段，值越小对应列越靠前
     */
    int order() default 0;

    /**
     * 列样式
     * default:默认样式 {@link IExcelService#defaultStyle()}
     * special: 特殊样式 {@link IExcelService#specialColumnStyle()}
     * @see IExcelService#STYLE_DEFAULT
     * @see IExcelService#STYLE_SPECIAL
     */
    String style() default IExcelService.STYLE_DEFAULT;
}
