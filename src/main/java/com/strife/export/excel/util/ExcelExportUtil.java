package com.strife.export.excel.util;

import com.strife.export.excel.annotation.Excel;
import com.strife.export.excel.async.core.AsyncExportManager;
import com.strife.export.excel.service.IExcelService;
import org.apache.poi.ss.usermodel.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: strife
 * @date: 2022/8/21
 */
public class ExcelExportUtil {


    /**
     * 一个sheet最多行数(除标题行)
     */
    private static final int SHEET_LINES = 3000;

    /**
     * 将数据转换为WorkBook
     *
     * @param excelServiceImpl 具体的Excel转换器
     * @param pojoClass        转换数据类型
     * @param dataList         数据
     * @param workbook         导出xls传{@link org.apache.poi.hssf.usermodel.HSSFWorkbook} 导出xlsx传 {@link org.apache.poi.xssf.streaming.SXSSFWorkbook}
     * @param id               文件ID
     * @return {@link Workbook}
     */
    public static Workbook exportExcel(Class<? extends IExcelService> excelServiceImpl,
                                       Class<?> pojoClass,
                                       List<?> dataList,
                                       Workbook workbook,
                                       Integer id) throws Exception {
        Constructor<? extends IExcelService> constructor = excelServiceImpl.getDeclaredConstructor(Workbook.class);
        IExcelService excelService = constructor.newInstance(workbook);
        // 默认单元格样式
        CellStyle defaultStyle = excelService.defaultStyle();
        // 特殊单元格样式
        CellStyle specialStyle = excelService.specialColumnStyle();
        int size = dataList.size();

        int sheets = getSheets(size);
        AsyncExportManager.INSTANCE.updateProgressBarInfo(id, 10D);
        double sheetTotalProgress = 80;
        double everySheetProgress = sheetTotalProgress / sheets;
        // sheet写入
        for (int curSheet = 0; curSheet < sheets; ++curSheet) {
            Sheet sheet = workbook.createSheet("sheet" + curSheet);
            Row row = sheet.createRow(0);
            Cell cell;
            // 所有要导出的字段
            List<Field> fieldList = getExcelFields(pojoClass);
            if (fieldList.size() == 0) {
                return workbook;
            }
            // 属性列根据order进行排序
            fieldList = orderFields(fieldList, pojoClass);

            // 标题行
            for (int titleColumn = 0; titleColumn < fieldList.size(); titleColumn++) {
                cell = row.createCell(titleColumn);
                cell.setCellStyle(excelService.getTitleStyle());

                Field field = pojoClass.getDeclaredField((fieldList.get(titleColumn)).getName());
                Excel excel = field.getAnnotation(Excel.class);
                cell.setCellValue(excel.value());
            }

            // 导出数据条数为0，只导出标题行
            if (size == 0) {
                return workbook;
            }

            // 当前sheet导出的行数
            int curSheetLines = SHEET_LINES;
            if (size % SHEET_LINES != 0 && curSheet == size / SHEET_LINES) {
                curSheetLines = size % 3000;
            }

            // 当前sheet要导出的数据
            List<?> tmpList = dataList.subList(curSheet * SHEET_LINES, curSheet * SHEET_LINES + curSheetLines);
            assignmentCell(tmpList, defaultStyle, specialStyle, sheet, fieldList, curSheetLines);

            // 设置每列列宽
            setColumWidth(pojoClass, sheet, fieldList);

            AsyncExportManager.INSTANCE.updateProgressBarInfo(id, everySheetProgress * (curSheet + 1));
        }
        AsyncExportManager.INSTANCE.updateProgressBarInfo(id, sheetTotalProgress);

        return workbook;
    }

    /**
     * 赋值单元格，设置具体值和样式
     *
     * @param dataList      数据列表
     * @param defaultStyle  默认单元格样式
     * @param specialStyle  特殊单元格样式
     * @param sheet         当前sheet
     * @param fieldList     字段列表
     * @param curSheetLines 当前sheet导出的行数
     */
    private static void assignmentCell(List<?> dataList, CellStyle defaultStyle, CellStyle specialStyle, Sheet sheet, List<Field> fieldList, int curSheetLines) throws NoSuchFieldException, IllegalAccessException {
        Cell cell;
        Row row;
        int w;
        for (w = 0; w < curSheetLines; w++) {
            // 创建行
            row = sheet.createRow(w + 1);
            Object obj = dataList.get(w);

            for (int c = 0; c < fieldList.size(); ++c) {
                // 创建单元格
                cell = row.createCell(c);
                cell.setCellType(CellType.STRING);
                Field filed = fieldList.get(c);
                Excel excel = filed.getAnnotation(Excel.class);
                if (IExcelService.STYLE_DEFAULT.equals(excel.style())) {
                    cell.setCellStyle(defaultStyle);
                } else {
                    if (specialStyle == null) {
                        throw new IllegalAccessException("please implement IExcelService.specialColumnStyle() and pass the implementation class");
                    }
                    cell.setCellStyle(specialStyle);
                }
                String filedName = filed.getName();
                Field field = obj.getClass().getDeclaredField(filedName);
                field.setAccessible(true);
                Object o = field.get(obj);
                cell.setCellValue(o != null ? o.toString() : "");
            }
        }
    }

    /**
     * 设置列宽
     *
     * @param pojoClass 转换数据类型
     * @param sheet     当前sheet
     * @param fieldList 转换字段列表
     * @throws NoSuchFieldException 字段未找到异常
     */
    private static void setColumWidth(Class<?> pojoClass, Sheet sheet, List<Field> fieldList) throws NoSuchFieldException {
        int w;
        for (w = 0; w < fieldList.size(); w++) {
            Field field = pojoClass.getDeclaredField((fieldList.get(w)).getName());
            Excel excel = field.getAnnotation(Excel.class);
            sheet.setColumnWidth(w, excel.width());
        }
    }

    /**
     * 计算sheet数量
     *
     * @param size 数量总数
     * @return sheet个数
     * @see ExcelExportUtil#SHEET_LINES
     */
    private static int getSheets(int size) {
        return size % SHEET_LINES != 0 ? size / SHEET_LINES + 1 : size / SHEET_LINES;
    }

    /**
     * 获取Excel注解标记的属性
     *
     * @param clazz 类名
     * @return 返回属性列表
     */
    private static List<Field> getExcelFields(Class<?> clazz) {
        try {
            List<Field> res = new ArrayList<>();
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Excel.class)) {
                    res.add(field);
                }
            }
            return res;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 根据排序字段对要导出的属性列表进行排序
     *
     * @param fields    要导出的属性列表
     * @param pojoClass 要导出的类
     * @return 从小到大排序好的属性列表
     * @see Excel#order()
     */
    private static List<Field> orderFields(List<Field> fields, Class<?> pojoClass) {
        return fields.stream().sorted((o1, o2) -> {
            try {
                Excel excel1 = pojoClass.getDeclaredField(o1.getName()).getAnnotation(Excel.class);
                Excel excel2 = pojoClass.getDeclaredField(o2.getName()).getAnnotation(Excel.class);
                return excel1.order() - excel2.order();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }
}
