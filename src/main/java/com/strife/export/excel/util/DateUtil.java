package com.strife.export.excel.util;

import org.apache.commons.lang3.time.FastDateFormat;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @description 日期工具类
 * @author: strife
 * @date: 2022/8/14
 */
public class DateUtil {

    public static final String DATE_STR = "yyyy-MM-dd HH:mm:ss";

    /**
     * 格式化字符串（默认格式）
     *
     * @param dateStr 日期字符串
     * @return Date
     */
    public static Date parse(String dateStr) {
        return parse(DATE_STR, dateStr);
    }

    /**
     * 格式化字符串
     *
     * @param pattern 字符串格式
     * @param dateStr 日期字符串
     * @return Date
     */
    public static Date parse(String pattern, String dateStr) {
        try {
            return FastDateFormat.getInstance(DATE_STR, TimeZone.getDefault(), Locale.CHINA).parse(dateStr);
        } catch (ParseException e) {
            throw new RuntimeException("日期转换失败, cause:", e);
        }
    }
}
