package com.strife.export.excel.util;

import com.strife.export.excel.async.core.AsyncExcelParam;
import com.strife.export.excel.async.core.AsyncExportManager;
import com.strife.export.excel.service.IExcelService;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.List;

/**
 * @description Excel工具类
 * @author: strife
 * @date: 2022/8/14
 */
public class ExcelUtil {
    private static final Logger logger = LoggerFactory.getLogger(ExcelUtil.class);

    /**
     * .xls后缀
     */
    private static final String SUFFIX_XLS = ".xls";

    /**
     * .xls导出对应content-type
     */
    private static final String CONTENT_TYPE_XLS = "application/vnd.ms-excel";

    /**
     * .xlsx后缀
     */
    private static final String SUFFIX_XLSX = ".xlsx";

    /**
     * .xlsx导出对应content-type
     */
    private static final String CONTENT_TYPE_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    public static void exportExcel(List<?> data,
                                   Class<?> clazz,
                                   Class<? extends IExcelService> handleClass,
                                   HttpServletResponse response,
                                   String fileName) throws Exception {
        exportExcel(data, clazz, handleClass, response, fileName, null);
    }

    /**
     * 导出Excel
     *
     * @param data        导出数据
     * @param clazz       导出数据类型
     * @param handleClass 导出具体处理器
     * @param response    响应体
     * @param fileName    导出文件名称
     * @param id          ID
     * @apiNote
     */
    public static void exportExcel(List<?> data,
                                   Class<?> clazz,
                                   Class<? extends IExcelService> handleClass,
                                   HttpServletResponse response,
                                   String fileName,
                                   Integer id) throws Exception {
        if (data == null) {
            return;
        }

        Workbook workbook = ExcelExportUtil.exportExcel(handleClass, clazz, data, new HSSFWorkbook(), id);

        if (!fileName.contains(SUFFIX_XLS)) {
            fileName = fileName + SUFFIX_XLS;
        }
        writeIO(workbook, response, fileName, CONTENT_TYPE_XLS, null);
    }

    public static void exportExcelXlsx(List<?> data,
                                       Class<?> clazz,
                                       Class<? extends IExcelService> handleClass,
                                       HttpServletResponse response,
                                       String fileName) throws Exception {
        exportExcelXlsx(data, clazz, handleClass, response, fileName, null);
    }

    /**
     * 导出Excel
     *
     * @param data        导出数据
     * @param clazz       导出数据类型
     * @param handleClass 导出具体处理器
     * @param response    响应体
     * @param fileName    导出文件名称
     * @param id          ID
     * @apiNote
     */
    public static void exportExcelXlsx(List<?> data,
                                       Class<?> clazz,
                                       Class<? extends IExcelService> handleClass,
                                       HttpServletResponse response,
                                       String fileName,
                                       Integer id) throws Exception {
        if (data == null) {
            return;
        }

        Workbook workbook = ExcelExportUtil.exportExcel(handleClass, clazz, data, new SXSSFWorkbook(), id);

        if (!fileName.contains(SUFFIX_XLSX)) {
            fileName = fileName + SUFFIX_XLSX;
        }
        writeIO(workbook, response, fileName, CONTENT_TYPE_XLSX, null);
    }

    /**
     * 导出Excel
     *
     * @param data        导出数据
     * @param clazz       导出数据类型
     * @param handleClass 导出具体处理器
     * @param fileName    导出文件名称
     * @apiNote
     */
    public static int exportExcelAsync(List<?> data,
                                       Class<?> clazz,
                                       Class<? extends IExcelService> handleClass,
                                       String fileName) throws Exception {
        if (data == null) {
            return 0;
        }
        AsyncExcelParam param = new AsyncExcelParam();
        param.setFunction(asyncExcelParam -> ExcelExportUtil.exportExcel(asyncExcelParam.getHandleClass(), asyncExcelParam.getClazz(), asyncExcelParam.getData(), new HSSFWorkbook(), asyncExcelParam.hashCode()));
        param.setData(data);
        param.setClazz(clazz);
        param.setHandleClass(handleClass);
        param.setFileName(fileName);
        return AsyncExportManager.INSTANCE.offer(param);
    }

    /**
     * 导出Excel
     *
     * @param data        导出数据
     * @param clazz       导出数据类型
     * @param handleClass 导出具体处理器
     * @param response    响应体
     * @param fileName    导出文件名称
     * @apiNote
     */
    public static void exportExcelXlsxAsync(List<?> data,
                                            Class<?> clazz,
                                            Class<? extends IExcelService> handleClass,
                                            HttpServletResponse response,
                                            String fileName) throws Exception {
        if (data == null) {
            return;
        }
        AsyncExcelParam param = new AsyncExcelParam();
        param.setFunction(asyncExcelParam -> ExcelExportUtil.exportExcel(asyncExcelParam.getHandleClass(), asyncExcelParam.getClazz(), asyncExcelParam.getData(), new SXSSFWorkbook(), asyncExcelParam.hashCode()));
        param.setData(data);
        param.setClazz(clazz);
        param.setHandleClass(handleClass);
        param.setFileName(fileName);
        AsyncExportManager.INSTANCE.offer(param);
    }

    public static void download(int hashCode, HttpServletResponse response) {
        if (AsyncExportManager.WAIT.containsKey(hashCode)) {
            logger.info("处于等待队列");
        } else if (AsyncExportManager.DOWNING.containsKey(hashCode)) {
            logger.info("处于下载队列");
        } else if (AsyncExportManager.FINISHED.containsKey(hashCode)) {
            logger.info("Workbook 生成完成");
            AsyncExcelParam param = AsyncExportManager.INSTANCE.getFinish(hashCode);
            Workbook workbook = param.getWorkbook();
            String contentType = workbook instanceof HSSFWorkbook ? CONTENT_TYPE_XLS : CONTENT_TYPE_XLSX;
            writeIO(workbook, response, param.getFileName(), contentType, hashCode);
        }
    }

    /**
     * 数据写入response
     *
     * @param workbook    excel数据
     * @param response    http响应
     * @param fileName    导出文件名
     * @param contentType 导出对应的content-type
     * @param id          文件ID
     */
    @SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
    private static void writeIO(Workbook workbook, HttpServletResponse response, String fileName, String contentType, Integer id) {
        ServletOutputStream outputStream = null;

        try {
            response.setCharacterEncoding("UTF-8");
            response.setContentType(contentType);
            response.setHeader("Content-Disposition", "attachment;filename=".concat(String.valueOf(URLEncoder.encode(fileName, "UTF-8"))));
            outputStream = response.getOutputStream();
            workbook.write(outputStream);
        } catch (Exception var8) {
            var8.printStackTrace();
        } finally {
            IOUtils.closeQuietly(outputStream);
            AsyncExportManager.INSTANCE.updateProgressBarInfo(id, 100D);
        }
    }
}
