package com.strife.export.excel.util;

import cn.hutool.core.collection.CollUtil;
import com.strife.export.excel.entity.CellData;
import jodd.util.StringTemplateParser;
import lombok.SneakyThrows;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 孟正浩
 * @date 2024-05-30
 **/
public class ExcelTemplateUtil {

    @SneakyThrows(Exception.class)
    public static Workbook fillTemplate(List<CellData> cellDatas, InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        if (CollUtil.isEmpty(cellDatas)) {
            return workbook;
        }
        Map<Integer, Map<Integer, List<CellData>>> cellDataMap = cellDatas.stream().collect(Collectors.groupingBy(CellData::getSheetNo, Collectors.groupingBy(CellData::getRowIndex)));
        for (Map.Entry<Integer, Map<Integer, List<CellData>>> entry : cellDataMap.entrySet()) {
            XSSFSheet sheet = workbook.getSheetAt(entry.getKey());
            for (Map.Entry<Integer, List<CellData>> rowEntry : entry.getValue().entrySet()) {
                XSSFRow row = sheet.getRow(rowEntry.getKey());
                for (CellData cellData : rowEntry.getValue()) {
                    XSSFCell cell = row.getCell(cellData.getColIndex());
                    Object data = cellData.getData();
                    setCellValue(cell, data);
                }
            }
        }
        return workbook;
    }

    private static void setCellValue(XSSFCell cell, Object data) {
        if (data instanceof Number) {
            cell.setCellValue(((Number) data).doubleValue());
        } else if (data instanceof Boolean) {
            cell.setCellValue((Boolean) data);
        } else if (data instanceof LocalDate) {
            cell.setCellValue((LocalDate) data);
        } else if (data instanceof Date) {
            cell.setCellValue((Date) data);
        } else {
            cell.setCellValue((String) data);
        }
    }


    @SneakyThrows(Exception.class)
    public static Workbook fillTemplate(Map<String, Object> varMap, InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        if (CollUtil.isEmpty(varMap)) {
            return workbook;
        }
        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            XSSFSheet sheet = workbook.getSheetAt(i);
            for (int j = 0; j < sheet.getLastRowNum(); j++) {
                XSSFRow row = sheet.getRow(j);
                for (int k = 0; k < row.getLastCellNum(); k++) {
                    XSSFCell cell = row.getCell(k);
                    try {
                        Object value = replaceVariables(cell.getStringCellValue(), varMap);
                        setCellValue(cell, value);
                    } catch (Exception ignore) {
                        // 非字符串直接忽略
                    }
                }
            }
        }
        return workbook;
    }

    private static Object replaceVariables(String input, Map<String, Object> varMap) {
        if (input == null || input.isEmpty()) {
            return input;
        }
        if (!input.contains(StringTemplateParser.DEFAULT_MACRO_START) || !input.contains(StringTemplateParser.DEFAULT_MACRO_END)) {
            return input;
        }
        if (isCompleteVariable(input)) {
            String variableName = input.substring(StringTemplateParser.DEFAULT_MACRO_START.length(),
                    input.length() - StringTemplateParser.DEFAULT_MACRO_END.length());
            return varMap.getOrDefault(variableName, variableName);
        }

        return StringTemplateParser.of(varName -> {
                    final Object value = varMap.get(varName);
                    if (value == null) {
                        return null;
                    }
                    return valueToString(value);
                })
                .apply(input);
    }

    /**
     * 判断字符串是否是一个完整的变量
     *
     * @param str 待判断的字符串
     * @return 如果字符串是一个完整的变量，则返回 true；否则返回 false
     */
    public static boolean isCompleteVariable(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }
        boolean startsWith = str.startsWith(StringTemplateParser.DEFAULT_MACRO_START);
        boolean endWith = str.endsWith(StringTemplateParser.DEFAULT_MACRO_END);
        int last = str.lastIndexOf(StringTemplateParser.DEFAULT_MACRO_START);
        return startsWith && endWith && last == 0;
    }

    /**
     * 将对象转换为字符串
     *
     * @param value 对象
     * @return 字符串表示
     */
    private static String valueToString(Object value) {
        if (value == null) {
            return "";
        }
        if (value instanceof Boolean) {
            return String.valueOf(value);
        } else if (value instanceof Number) {
            return String.valueOf(value);
        } else if (value instanceof Date) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.format((Date) value);
        }
        return String.valueOf(value);
    }
}
