package com.strife.export.excel.async.core;

import com.strife.export.excel.entity.ProgressBarInfo;
import com.strife.export.excel.service.IExcelService;
import lombok.Data;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;

/**
 * @author 孟正浩
 * @date 2024-05-08
 **/
@Data
public class AsyncExcelParam {

    private AsyncExcelFunction function;
    private List<?> data;
    private Class<?> clazz;
    private Class<? extends IExcelService> handleClass;
    private HttpServletResponse response;
    private String fileName;
    private Workbook workbook;
    private ProgressBarInfo progressBarInfo;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AsyncExcelParam)) {
            return false;
        }
        AsyncExcelParam param = (AsyncExcelParam) o;
        return Objects.equals(function, param.function) && Objects.equals(data, param.data) && Objects.equals(clazz, param.clazz) && Objects.equals(handleClass, param.handleClass) && Objects.equals(response, param.response) && Objects.equals(fileName, param.fileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(function, data, clazz, handleClass, response, fileName);
    }
}
