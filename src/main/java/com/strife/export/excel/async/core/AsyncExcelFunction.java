package com.strife.export.excel.async.core;

import org.apache.poi.ss.usermodel.Workbook;

/**
 * 异步执行Function
 *
 * @author 孟正浩
 * @date 2024-04-30
 **/
@FunctionalInterface
public interface AsyncExcelFunction {

    /**
     * 执行导出
     *
     * @param asyncExcelParam 异步导出参数
     * @return 返回WorkBook
     * @throws Exception 导出异常
     */
    Workbook execute(AsyncExcelParam asyncExcelParam) throws Exception;
}