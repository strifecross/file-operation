package com.strife.export.excel.async.core;

import lombok.Data;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayDeque;
import java.util.Map;
import java.util.concurrent.*;

/**
 * 异步导出队列
 *
 * @author 孟正浩
 * @date 2024-04-30
 **/
@Data
public class AsyncExportManager {
    private static final Logger logger = LoggerFactory.getLogger(AsyncExportManager.class);

    private static ArrayDeque<AsyncExcelParam> DEQUE = new ArrayDeque<>();
    public static Map<Integer, AsyncExcelParam> WAIT = new ConcurrentHashMap<>();
    public static Map<Integer, AsyncExcelParam> DOWNING = new ConcurrentHashMap<>();
    public static Map<Integer, AsyncExcelParam> FINISHED = new ConcurrentHashMap<>();

    public static Map<Integer, Double> PROGRESS = new ConcurrentHashMap<>();

    private static Semaphore semaphore = new Semaphore(10);

    public static AsyncExportManager INSTANCE = new AsyncExportManager();

    public AsyncExportManager() {
        new Holder();
    }

    public int offer(AsyncExcelParam param) {
        int hashCode = param.hashCode();
        DEQUE.offer(param);
        WAIT.put(hashCode, param);
        return hashCode;
    }

    public AsyncExcelParam poll() {
        AsyncExcelParam param = DEQUE.poll();
        if (param != null) {
            int hashCode = param.hashCode();
            WAIT.remove(hashCode);
            DOWNING.put(hashCode, param);
            PROGRESS.put(hashCode, 0D);
        }
        return param;
    }

    public void finish(AsyncExcelParam param) {
        int hashCode = param.hashCode();
        DOWNING.remove(hashCode);
        FINISHED.put(hashCode, param);
        PROGRESS.put(hashCode, 90D);
    }

    public AsyncExcelParam getFinish(int hashCode) {
        AsyncExcelParam param = FINISHED.get(hashCode);
        FINISHED.remove(hashCode);
        return param;
    }

    /**
     * 更新进度信息
     */
    public void updateProgressBarInfo(Integer hashCode, Double progress) {
        if (hashCode == null) {
            return;
        }
        PROGRESS.put(hashCode, progress);
        logger.info("文件ID: {}, 下载进度: {}%", hashCode, progress);
    }

    public static class Holder {
        private static final Integer SLEEP = 1;

        public Holder() {
            ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
            scheduler.scheduleWithFixedDelay(() -> {
                if (semaphore.tryAcquire()) {
                    try {
                        AsyncExcelParam asyncExcelParam = INSTANCE.poll();
                        if (asyncExcelParam != null) {
                            try {
                                Workbook workbook = asyncExcelParam.getFunction().execute(asyncExcelParam);
                                asyncExcelParam.setWorkbook(workbook);
                                INSTANCE.finish(asyncExcelParam);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } finally {
                        semaphore.release();
                    }

                }
            }, 0, SLEEP, TimeUnit.SECONDS);
        }
    }
}
