package com.strife.export.excel.async.config;

import cn.hutool.core.thread.NamedThreadFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author 孟正浩
 * @date 2024-04-30
 **/
@Configuration
public class AsyncConfiguration {

    /**
     * 异步导出线程池
     */
    @Bean
    public ThreadPoolExecutor asyncExportExecutors() {
        return new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors() / 2, Runtime.getRuntime().availableProcessors(),
                10, TimeUnit.SECONDS, new ArrayBlockingQueue<>(1000),
                new NamedThreadFactory("async-export-", false),
                new ThreadPoolExecutor.CallerRunsPolicy());
    }
}
