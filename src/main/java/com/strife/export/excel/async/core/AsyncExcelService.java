package com.strife.export.excel.async.core;

import cn.hutool.core.collection.CollectionUtil;
import com.strife.export.excel.entity.ProgressBarInfo;
import com.strife.poi.easyexcel.core.AsyncImportManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * @author 孟正浩
 * @date 2024-05-08
 **/
@Service
public class AsyncExcelService {
    private static final Logger logger = LoggerFactory.getLogger(AsyncExcelService.class);

    private static final String PROGRESS_BAR = "progressBar";
    public static final int RETRY_TIME = 5;

    static {
        new Holder();
    }

    /**
     * 容器，保存连接，用于输出返回
     */
    private static final Map<Integer, SseEmitter> SSE_CACHE = new ConcurrentHashMap<>();

    public SseEmitter connect(Integer id) {
        // 设置超时时间，0表示不过期。默认30秒，超过时间未完成会抛出异常：AsyncRequestTimeoutException
        SseEmitter emitter = new SseEmitter(0L);

        // 注册回调函数，处理服务器向客户端推送的消息
        emitter.onCompletion(completionCallBack(id));
        emitter.onTimeout(timeoutCallBack(id));
        emitter.onError(errorCallBack(id));
        SSE_CACHE.put(id, emitter);
        logger.info("创建新的sse连接，当前文件ID：{}", id);
        try {
            ProgressBarInfo progressBarInfo = new ProgressBarInfo();
            progressBarInfo.setId(id);
            emitter.send(SseEmitter.event().id(PROGRESS_BAR).data(progressBarInfo));
        } catch (IOException e) {
            logger.error("AsyncExcelService[createSseConnect]: 创建长链接异常，文件ID:{}", id, e);
            throw new RuntimeException("创建连接异常！", e);
        }
        return emitter;
    }

    public void close(Integer id) {
        SseEmitter sseEmitter = SSE_CACHE.get(id);
        if (sseEmitter != null) {
            sseEmitter.complete();
            removeId(id);
        }
    }

    /**
     * 长链接完成后回调接口(即关闭连接时调用)
     **/
    private Runnable completionCallBack(Integer id) {
        return () -> {
            logger.info("结束连接：{}", id);
            removeId(id);
        };
    }

    /**
     * 连接超时时调用
     **/
    private Runnable timeoutCallBack(Integer id) {
        return () -> {
            logger.info("连接超时：{}", id);
            removeId(id);
        };
    }

    /**
     * 推送消息异常时，回调方法
     **/
    private Consumer<Throwable> errorCallBack(Integer id) {
        return throwable -> {
            logger.error("AsyncExcelService[errorCallBack]：连接异常,文件ID:{}", id);

            // 推送消息失败后，每隔10s推送一次，推送5次
            for (int i = 0; i < 5; i++) {
                try {
                    Thread.sleep(10000);
                    SseEmitter sseEmitter = SSE_CACHE.get(id);
                    if (sseEmitter == null) {
                        logger.error("AsyncExcelService[errorCallBack]：第{}次消息重推失败,未获取到 {} 对应的长链接", i + 1, id);
                        continue;
                    }
                    sseEmitter.send("失败后重新推送");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * 移除用户连接
     **/
    private void removeId(Integer id) {
        SSE_CACHE.remove(id);
        logger.info("AsyncExcelService[removeSse]:移除文件ID：{}", id);
    }

    private static class Holder {

        private static final Integer SLEEP = 1;

        public Holder() {
            ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
            scheduler.scheduleWithFixedDelay(() -> {
                for (Map.Entry<Integer, Double> entry : AsyncExportManager.PROGRESS.entrySet()) {
                    ProgressBarInfo progressBarInfo = new ProgressBarInfo();
                    progressBarInfo.setId(entry.getKey());
                    progressBarInfo.setProgressBar(entry.getValue());
                    sendProgress(entry.getKey(), progressBarInfo);
                }
                for (Map.Entry<Integer, Double> entry : AsyncImportManager.PROGRESS.entrySet()) {
                    ProgressBarInfo progressBarInfo = new ProgressBarInfo();
                    progressBarInfo.setId(entry.getKey());
                    progressBarInfo.setProgressBar(entry.getValue());
                    sendProgress(entry.getKey(), progressBarInfo);
                }
            }, 0, SLEEP, TimeUnit.SECONDS);
        }

        /**
         * 推送进度信息到前端
         */
        public void sendProgress(Integer id, ProgressBarInfo progressBarInfo) {
            if (CollectionUtil.isEmpty(SSE_CACHE)) {
                return;
            }
            SseEmitter sseEmitter = SSE_CACHE.get(id);
            if (sseEmitter != null) {
                SseEmitter.SseEventBuilder sendData = SseEmitter.event().id(PROGRESS_BAR).data(progressBarInfo, MediaType.APPLICATION_JSON);
                try {
                    sseEmitter.send(sendData);
                } catch (IOException e) {
                    // 推送消息失败，记录错误日志，进行重推
                    logger.error("AsyncExcelService[sendMsgToClient]: 推送消息失败：{},尝试进行重推", progressBarInfo, e);
                    boolean isSuccess = true;
                    // 推送消息失败后，每隔10s推送一次，推送5次
                    for (int i = 0; i < RETRY_TIME; i++) {
                        try {
                            Thread.sleep(10000);
                            sseEmitter = SSE_CACHE.get(id);
                            if (sseEmitter == null) {
                                logger.error("AsyncExcelService[sendMsgToClient]：{}的第{}次消息重推失败，未创建长链接", id, i + 1);
                                continue;
                            }
                            sseEmitter.send(sendData);
                        } catch (Exception ex) {
                            logger.error("AsyncExcelService[sendMsgToClient]：{}的第{}次消息重推失败", id, i + 1, ex);
                            continue;
                        }
                        logger.info("AsyncExcelService[sendMsgToClient]：{}的第{}次消息重推成功,{}", id, i + 1, progressBarInfo);
                        return;
                    }
                }
            }
        }
    }

}
