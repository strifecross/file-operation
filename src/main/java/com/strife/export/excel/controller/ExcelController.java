package com.strife.export.excel.controller;

import com.strife.export.excel.async.core.AsyncExcelService;
import com.strife.export.excel.entity.Student;
import com.strife.export.excel.service.impl.ExcelServiceImpl;
import com.strife.export.excel.util.ExcelUtil;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author: strife
 * @date: 2022/8/21
 */
@RestController
@RequestMapping("/excel")
public class ExcelController {

    @Resource
    private AsyncExcelService asyncExcelService;

    @GetMapping("/export")
    public void exportExcel(HttpServletResponse response) throws Exception {
        ExcelUtil.exportExcel(mockData(), Student.class, ExcelServiceImpl.class, response, "export.xls");
    }

    @GetMapping("/exportXlsx")
    public void exportExcelXlsx(HttpServletResponse response) throws Exception {
        ExcelUtil.exportExcelXlsx(mockData(), Student.class, ExcelServiceImpl.class, response, "export.xlsx");
    }

    @CrossOrigin
    @GetMapping("/asyncExport")
    public int asyncExportExcel() throws Exception {
        return ExcelUtil.exportExcelAsync(mockData(), Student.class, ExcelServiceImpl.class, "export.xls");
    }

    @CrossOrigin
    @GetMapping("/download")
    public void download(@RequestParam Integer hashCode, HttpServletResponse response) throws Exception {
        ExcelUtil.download(hashCode, response);
    }

    /**
     * 查询导出进度
     */
    @CrossOrigin
    @GetMapping(value = "/progressBar", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter progressBar(@RequestParam Integer id) throws IOException {
        return asyncExcelService.connect(id);
    }

    /**
     * 断开sse连接
     */
    @CrossOrigin
    @GetMapping(value = "/closeSse")
    public void closeSse(@RequestParam Integer id) throws IOException {
        asyncExcelService.close(id);
    }

    private static List<Student> mockData() {
        List<Student> res = new ArrayList<>();
        for (int i = 1; i <= 400000; i++) {
            res.add(new Student(String.valueOf(i), "name" + i, (i & 1) == 0 ? "男" : "女", new Random().nextInt(30)));
        }
        return res;
    }
}
