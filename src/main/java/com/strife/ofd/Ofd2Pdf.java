package com.strife.ofd;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.ofdrw.converter.ConvertHelper;
import org.ofdrw.graphics2d.OFDGraphicsDocument;
import org.ofdrw.graphics2d.OFDPageGraphics2D;

import java.io.IOException;
import java.nio.file.Paths;

/**
 * @author 孟正浩
 * @date 2024-05-16
 **/
public class Ofd2Pdf {

    public static void ofd2Pdf(String ofdPath, String pdfPath) {
        ConvertHelper.toPdf(Paths.get(ofdPath), Paths.get(pdfPath));
    }

    public static void pdf2Ofd(String pdfPath, String ofdPath) throws IOException {
        try (OFDGraphicsDocument ofdDoc = new OFDGraphicsDocument(Paths.get(ofdPath));
             PDDocument pdfDoc = PDDocument.load(Paths.get(pdfPath).toFile())) {
            PDFRenderer pdfRender = new PDFRenderer(pdfDoc);
            for (int pageIndex = 0; pageIndex < pdfDoc.getNumberOfPages(); pageIndex++) {
                PDRectangle pdfPageSize = pdfDoc.getPage(pageIndex).getBBox();
                OFDPageGraphics2D ofdPageG2d = ofdDoc.newPage(pdfPageSize.getWidth(), pdfPageSize.getHeight());
                pdfRender.renderPageToGraphics(pageIndex, ofdPageG2d);
            }
        }
    }
}
