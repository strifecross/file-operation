package com.strife.utils;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.ReaderProperties;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * @author: strife
 * @date: 2023/10/2
 */
public class FileReadUtil {

    /**
     * 读取txt文件
     */
    public static String readTxtFile(File file) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try (FileInputStream inputStream = new FileInputStream(file)) {
            byte[] bytes = new byte[1024];
            int len;
            while ((len = inputStream.read(bytes)) != -1) {
                stringBuilder.append(new String(bytes, 0, len));
            }
        } catch (IOException e) {
            throw new IOException(e);
        }
        return stringBuilder.toString();
    }

    /**
     * 读取pdf文件
     */
    public static String readPdfFile(File file) throws IOException {
        return readPdfFile(file, null);
    }

    /**
     * 读取pdf文件
     *
     * @param file     pdf文件
     * @param password pdf文件密码
     */
    public static String readPdfFile(File file, String password) throws IOException {
        StringBuilder builder = new StringBuilder();
        try (InputStream inputStream = new FileInputStream(file)) {
            ReaderProperties properties = new ReaderProperties();
            if (StringUtils.isNotEmpty(password)) {
                properties.setOwnerPassword(password.getBytes(StandardCharsets.UTF_8));
            }
            PdfReader reader = new PdfReader(properties, inputStream);
            int pages = reader.getNumberOfPages();
            for (int i = 1; i <= pages; i++) {
                String text = PdfTextExtractor.getTextFromPage(reader, i);
                builder.append(text);
            }
        }
        return builder.toString();
    }
}
