package com.strife.utils;

import cn.hutool.core.collection.CollectionUtil;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.HttpHeaders;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author: strife
 * @date: 2023/11/19
 */
public class FileTypeUtil {

    private static final Logger logger = LoggerFactory.getLogger(FileTypeUtil.class);

    private static final Map<String, List<String>> MIME_TYPE_MAP;

    static {
        MIME_TYPE_MAP = new HashMap<>();
        try {
            SAXReader saxReader = new SAXReader();
            Document document = saxReader.read(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                    "mime-types.xml"));

            Element rootElement = document.getRootElement();
            List<Element> mimeTypeElements = rootElement.elements("mime-type");
            for (Element mimeTypeElement : mimeTypeElements) {
                String type = mimeTypeElement.attributeValue("type");
                List<Element> globElements = mimeTypeElement.elements("glob");
                List<String> fileTypeList = new ArrayList<>(globElements.size());
                for (Element globElement : globElements) {
                    String fileType = globElement.getTextTrim();
                    fileTypeList.add(fileType);
                }
                MIME_TYPE_MAP.put(type, fileTypeList);
            }
        } catch (DocumentException e) {
            logger.error("mime-types.xml文件解析异常", e);
        }

    }


    /**
     * 获取类型
     * @param file
     */
    public static String getFileMimeType(File file) {
        AutoDetectParser parser = new AutoDetectParser();
        parser.setParsers(new HashMap<>());
        Metadata metadata = new Metadata();
        metadata.add(TikaCoreProperties.RESOURCE_NAME_KEY, file.getName());
        metadata.set(Metadata.CONTENT_LENGTH, Long.toString(file.length()));
        try (InputStream stream = new FileInputStream(file)) {
            parser.parse(stream, new DefaultHandler(), metadata, new ParseContext());
        }catch (IOException | SAXException | TikaException e){
            logger.error("文件的MimeType类型解析失败，原因: ", e);
            throw new RuntimeException();
        }
        return metadata.get(HttpHeaders.CONTENT_TYPE);
    }

    /**
     * 获取文件的真实类型, 全为小写
     */
    public static List<String> getFileRealTypeList(File file) {
        String fileMimeType = getFileMimeType(file);
        logger.info("fileMimeType:{}", fileMimeType);
        return getFileRealTypeList(fileMimeType);
    }

    /**
     * 获取文件的真实类型, 全为小写
     * 匹配到多个取第一个
     */
    public static String getFileRealType(File file) {
        String fileMimeType = getFileMimeType(file);
        logger.info("fileMimeType:{}", fileMimeType);
        List<String> list = getFileRealTypeList(fileMimeType);
        if (CollectionUtil.isEmpty(list)) {
            return getFileSuffix(file);
        }
        return list.get(0);
    }


    /**
     * 根据文件的mime类型获取文件的真实扩展名集合
     *
     * @param mimeType 文件的mime 类型
     * @return 文件的扩展名集合
     */
    public static List<String> getFileRealTypeList(String mimeType) {
        if (ObjectUtils.isEmpty(mimeType)) {
            return Collections.emptyList();
        }

        List<String> fileTypeList = MIME_TYPE_MAP.get(mimeType.replace(" ", ""));
        if (fileTypeList == null) {
            logger.info("mimeType:{}, FileTypeList is null", mimeType);
            return Collections.emptyList();
        }
        return fileTypeList;
    }

    private static String getFileSuffix(File file) {
        String name = file.getName();
        return name.substring(name.lastIndexOf(".") + 1);
    }

}
