package com.strife.encrypt.factory;

import com.strife.encrypt.service.AbstractEncryptService;
import com.strife.enums.OfficeEncryptServiceEnum;
import com.strife.utils.FileTypeUtil;

import java.io.File;

/**
 * Office文件加密、解密、只读
 *
 * @author: strife
 * @date: 2023/11/26
 */
public class OfficeFileEncryptFactory {

    private OfficeFileEncryptFactory() {
    }

    public static void encrypt(File srcFile, String password) {
        encrypt(srcFile, null, password);
    }

    public static void encrypt(File srcFile, File destFile, String password) {
        String realType = FileTypeUtil.getFileRealType(srcFile);
        AbstractEncryptService encryptService = OfficeEncryptServiceEnum.getOfficeEncryptService(realType);
        if (destFile == null) {
            encryptService.encryptFile(srcFile, password);
        } else {
            encryptService.encryptFile(srcFile, destFile, password);
        }
    }

    /**
     * Office文件解密
     * docx文件返回 XWPFDocument
     * xlsx文件返回 Workbook
     * pptx文件返回 XMLSlideShow
     */
    public static Object decrypt(File encryptFile, String password) {
        String realType = FileTypeUtil.getFileRealType(encryptFile);
        AbstractEncryptService encryptService = OfficeEncryptServiceEnum.getOfficeEncryptService(realType);
        return encryptService.decrypt(encryptFile, password);
    }

    public static void onlyRead(File srcFile, String password) {
        onlyRead(srcFile, null, password);
    }

    public static void onlyRead(File srcFile, File destFile, String password) {
        String realType = FileTypeUtil.getFileRealType(srcFile);
        AbstractEncryptService encryptService = OfficeEncryptServiceEnum.getOfficeEncryptService(realType);
        if (destFile == null) {
            encryptService.onlyReadFile(srcFile, password);
        } else {
            encryptService.onlyReadFile(srcFile, destFile, password);
        }
    }


}
