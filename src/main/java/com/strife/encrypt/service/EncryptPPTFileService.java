package com.strife.encrypt.service;

import com.strife.encrypt.util.FileEncryptUtil;
import org.apache.poi.xslf.usermodel.XMLSlideShow;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author: strife
 * @date: 2023/11/19
 */
public class EncryptPPTFileService extends AbstractEncryptService {

    @Override
    public void encryptFile(File srcFile, File destFile, String password) {
        FileEncryptUtil.encryptOffice(srcFile, destFile, password);
    }


    @Override
    public void onlyReadFile(File srcFile, File destFile, String password) {
        throw new UnsupportedOperationException("PPT暂时不支持设置只读");
    }

    @Override
    public Object parseInputStream(InputStream inputStream) throws IOException {
        return new XMLSlideShow(inputStream);
    }
}
