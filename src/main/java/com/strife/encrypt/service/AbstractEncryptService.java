package com.strife.encrypt.service;

import org.apache.commons.io.FileUtils;
import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.security.GeneralSecurityException;

/**
 * @author: strife
 * @date: 2023/11/15
 */
public abstract class AbstractEncryptService {

    private static final Logger logger = LoggerFactory.getLogger(AbstractEncryptService.class);

    /**
     * 加密文件，输入密码才可打开，源文件进行加密
     *
     * @param file     文件
     * @param password 密码
     */
    public void encryptFile(File file, String password) {
        File tmpFile = null;
        try {
            tmpFile = Files.createTempFile("encrypt", ".docx").toFile();
        } catch (IOException e) {
            logger.error("创建临时文件失败", e);
        }
        if (tmpFile == null) {
            return;
        }
        encryptFile(file, tmpFile, password);
        try {
            FileUtils.delete(file);
            FileUtils.moveFile(tmpFile, file);
        } catch (IOException e) {
            logger.error("文件重命名失败", e);
        }
    }

    /**
     * 加密文件，输入密码才可打开，源文件进行加密
     *
     * @param srcFile  文件
     * @param destFile 目标文件
     * @param password 密码
     */
    public abstract void encryptFile(File srcFile, File destFile, String password);

    /**
     * 文件只读，输入密码才可编辑，源文件进行加密
     *
     * @param file     文件
     * @param password 密码
     */
    public void onlyReadFile(File file, String password) {
        File tmpFile = null;
        try {
            tmpFile = Files.createTempFile("encrypt", ".docx").toFile();
        } catch (IOException e) {
            logger.error("创建临时文件失败", e);
        }
        if (tmpFile == null) {
            return;
        }
        onlyReadFile(file, tmpFile, password);
        try {
            FileUtils.delete(file);
            FileUtils.moveFile(tmpFile, file);
        } catch (IOException e) {
            logger.error("文件重命名失败", e);
        }
    }

    /**
     * 文件只读，输入密码才可编辑，源文件进行加密
     *
     * @param srcFile  文件
     * @param destFile 目标文件
     * @param password 密码
     */
    public abstract void onlyReadFile(File srcFile, File destFile, String password);


    /**
     * 文件解密
     *
     * @param encryptFile 加密文件
     * @param password    密码
     * @return 返回解密后的对象
     */
    public Object decrypt(File encryptFile, String password) {
        try (POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(encryptFile))) {
            EncryptionInfo info = new EncryptionInfo(fs);
            Decryptor decryptor = Decryptor.getInstance(info);
            try {
                if (!decryptor.verifyPassword(password)) {
                    throw new RuntimeException("Unable to process: document is encrypted");
                }
                return parseInputStream(decryptor.getDataStream(fs));
            } catch (GeneralSecurityException ex) {
                throw new RuntimeException("Unable to process encrypted document", ex);
            }
        } catch (IOException e) {
            logger.error("Encrypt docx file failed, {}", e.getMessage());
        }
        return null;
    }

    /**
     * 处理解密后的流
     *
     * @param inputStream 机密后的流
     * @return 返回对应对象
     */
    public abstract Object parseInputStream(InputStream inputStream) throws IOException;
}
