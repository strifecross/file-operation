package com.strife.encrypt.service;

import com.strife.encrypt.util.FileEncryptUtil;
import org.apache.poi.poifs.crypt.HashAlgorithm;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * @author: strife
 * @date: 2023/11/15
 */
public class EncryptDocxFileService extends AbstractEncryptService {

    private static final Logger logger = LoggerFactory.getLogger(EncryptDocxFileService.class);


    @Override
    public void encryptFile(File srcFile, File destFile, String password) {
        FileEncryptUtil.encryptOffice(srcFile, destFile, password);
    }

    @Override
    public void onlyReadFile(File srcFile, File destFile, String password) {
        try (FileInputStream is = new FileInputStream(srcFile);
             XWPFDocument doc = new XWPFDocument(is);
             OutputStream os = new FileOutputStream(destFile);) {
            doc.enforceFillingFormsProtection(password, HashAlgorithm.md5);
            doc.write(os);
            os.flush();
        } catch (Exception e) {
            logger.error("Encrypt docx file failed, {}", e.getMessage());
        }
    }

    @Override
    public Object parseInputStream(InputStream inputStream) throws IOException {
        return new XWPFDocument(inputStream);
    }


}
