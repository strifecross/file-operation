package com.strife.encrypt.service;

import com.strife.encrypt.util.FileEncryptUtil;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * @author: strife
 * @date: 2023/11/19
 */
public class EncryptExcelFileService extends AbstractEncryptService {
    private static final Logger logger = LoggerFactory.getLogger(EncryptExcelFileService.class);

    @Override
    public void encryptFile(File srcFile, File destFile, String password) {
        FileEncryptUtil.encryptOffice(srcFile, destFile, password);
    }


    @Override
    public void onlyReadFile(File srcFile, File destFile, String password) {
        Workbook workbook;
        try {
            workbook = WorkbookFactory.create(srcFile);
        } catch (IOException e) {
            logger.error("读取Excel文件失败");
            throw new RuntimeException(e);
        }
        int sheets = workbook.getNumberOfSheets();
        for (int i = 0; i < sheets; i++) {
            Sheet sheet = workbook.getSheetAt(i);
            sheet.protectSheet(password);
        }
        try (OutputStream outputStream = new FileOutputStream(destFile)) {
            workbook.write(outputStream);
            outputStream.flush();
        } catch (IOException e) {
            logger.error("写入文件失败", e);
        }
    }

    @Override
    public Object parseInputStream(InputStream inputStream) throws IOException {
        return WorkbookFactory.create(inputStream);
    }
}
