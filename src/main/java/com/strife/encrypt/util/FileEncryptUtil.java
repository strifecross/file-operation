package com.strife.encrypt.util;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.poifs.crypt.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.security.GeneralSecurityException;

/**
 * @author: strife
 * @date: 2023/11/19
 */
public class FileEncryptUtil {

    private static final Logger logger = LoggerFactory.getLogger(FileEncryptUtil.class);


    /**
     * docx、xlsx、pptx文件加密
     */
    public static void encryptOffice(File srcFile, File destFile, String password) {
        try (POIFSFileSystem fs = new POIFSFileSystem()) {
            EncryptionInfo info = new EncryptionInfo(EncryptionMode.agile);
            // EncryptionInfo info = new EncryptionInfo(EncryptionMode.agile, CipherAlgorithm.aes192, HashAlgorithm.sha384, -1, -1, null);
            Encryptor enc = info.getEncryptor();
            enc.confirmPassword(password);
            // Read in an existing OOXML file and write to encrypted output stream
            // don't forget to close the output stream otherwise the padding bytes aren't added
            try (OPCPackage opc = OPCPackage.open(srcFile, PackageAccess.READ_WRITE);
                 OutputStream os = enc.getDataStream(fs)) {
                opc.save(os);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            // Write out the encrypted version
            try (FileOutputStream fos = new FileOutputStream(destFile)) {
                fs.writeFilesystem(fos);
            }
        } catch (IOException e) {
            logger.error("Encrypt docx file failed, {}", e.getMessage());
        }
    }

}
