package com.strife.convert.util;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Word转Pdf工具类
 *
 * @author: strife
 * @date: 2023/11/12
 */
public class ConvertUtil {

    private static final Logger logger = LoggerFactory.getLogger(ConvertUtil.class);

    public static void reNameFile(String srcFileName, String destFileName) {
        File srcFile= new File(srcFileName);
        if (!srcFile.exists()) {
            logger.error("文件不存在, {}", srcFileName);
            return;
        }
        try {
            FileUtils.moveFile(srcFile, new File(destFileName));
        } catch (IOException e) {
            logger.error("文件重命名失败", e);
        }
    }

}
