package com.strife.convert.constant;

/**
 * @author: strife
 * @date: 2023/11/12
 */
public class ConvertConstant {

    public static final String PROGRAM = "program";
    public static final String SHELL = "shell";
}
