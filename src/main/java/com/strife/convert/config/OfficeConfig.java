package com.strife.convert.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author: strife
 * @date: 2023/11/12
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "office")
public class OfficeConfig {

    private String convertType;
    private String home;
    private String ports;
    private int timeout;
}
