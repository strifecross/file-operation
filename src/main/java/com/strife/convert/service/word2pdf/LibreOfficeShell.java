package com.strife.convert.service.word2pdf;

import com.strife.convert.config.OfficeConfig;
import com.strife.convert.util.ConvertUtil;
import com.strife.file.local.utils.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jodconverter.core.util.OSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.*;
import java.util.stream.Collectors;

/**
 * @author: strife
 * @date: 2023/11/12
 */
@Component
@ConditionalOnExpression("'${office.convertType}'.equals('shell')")
public class LibreOfficeShell implements IConvertService {
    private static final Logger logger = LoggerFactory.getLogger(LibreOfficeShell.class);

    @Resource
    private OfficeConfig officeConfig;

    @Override
    public void word2Pdf(String wordFileName, String pdfName) {
        String pdfPath = FileUtils.getFilePath(pdfName);
        String command = officeConfig.getHome() + " --headless --convert-to pdf " + wordFileName + " --outdir " + pdfPath;
        if (OSUtils.IS_OS_WINDOWS) {
            command = "cmd /c " + command;
        } else {
            command = "sh -c " + command;
        }
        executeCommand(command);
        String srcPdfName = pdfPath + FileUtils.getFileName(wordFileName) + ".pdf";
        ConvertUtil.reNameFile(srcPdfName, pdfName);
    }

    private static void executeCommand(String command) {
        Process process = null;
        try {
            logger.debug("command execute : {}", command);
            process = Runtime.getRuntime().exec(command);
           InputStream errorStream = process.getErrorStream();
           if (errorStream != null) {
               BufferedReader reader = new BufferedReader(new InputStreamReader(errorStream));
               String errInfo = reader.lines().collect(Collectors.joining(System.lineSeparator()));
               if (StringUtils.isNotEmpty(errInfo)) {
                   logger.error("command execute error, errorInfo: {}", errInfo);
               }
           }
        } catch (IOException e) {
            logger.error("command execute {} error", command, e);
        }
        int exitStatus;
        try {
            exitStatus = process.waitFor();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (exitStatus != 0) {
            logger.error("command execute exitStatus {}", exitStatus);
        }
        process.destroy();
    }
}
