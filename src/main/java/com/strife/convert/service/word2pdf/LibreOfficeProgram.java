package com.strife.convert.service.word2pdf;

import org.jodconverter.core.office.OfficeException;
import org.jodconverter.local.LocalConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author: strife
 * @date: 2023/11/12
 */
@Component
@ConditionalOnExpression("'${office.convertType}'.equals('program')")
public class LibreOfficeProgram implements IConvertService {
    private static final Logger logger = LoggerFactory.getLogger(LibreOfficeProgram.class);
    @Override
    public void word2Pdf(String wordFileName, String pdfName) {
        try {
            LocalConverter.builder()
                    .build()
                    .convert(new File(wordFileName))
                    .to(new File(pdfName))
                    .execute();
        } catch (OfficeException e) {
            logger.error("Word转Pdf失败", e);
        }
    }
}
