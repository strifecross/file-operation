package com.strife.convert.service.word2pdf;

import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author: strife
 * @date: 2023/12/2
 */
public class AsposeConvertService implements IConvertService{
    private static final Logger logger = LoggerFactory.getLogger(AsposeConvertService.class);
    @Override
    public void word2Pdf(String wordFileName, String pdfName) {
        try (InputStream inputStream = new FileInputStream(wordFileName);
             OutputStream outputStream = new FileOutputStream(pdfName)){
            Document doc = new Document(inputStream);
            doc.save(outputStream, SaveFormat.PDF);
        } catch (Exception e) {
            logger.error("Word转PDf失败: ", e);
        }
    }


    static {
        try {
            InputStream is = AsposeConvertService.class.getClassLoader().getResourceAsStream("license.xml");
            License aposeLic = new License();
            aposeLic.setLicense(is);
        } catch (Exception e) {
            logger.error("License获取失败");
        }
    }
}
