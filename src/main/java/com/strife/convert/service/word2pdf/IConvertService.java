package com.strife.convert.service.word2pdf;

/**
 * @author: strife
 * @date: 2023/11/12
 */
public interface IConvertService {

    /**
     * Word转PDF
     *
     * @param wordFileName Word文件路径
     * @param pdfName      pdf文件路径
     */
    void word2Pdf(String wordFileName, String pdfName);
}
