package com.strife.convert.service.word2pdf;

import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author: strife
 * @date: 2023/12/2
 */
public class PoiConvertService implements IConvertService{
    private static final Logger logger = LoggerFactory.getLogger(PoiConvertService.class);
    @Override
    public void word2Pdf(String wordFileName, String pdfName) {
        try (InputStream inputStream = new FileInputStream(wordFileName);
             OutputStream out = new FileOutputStream(pdfName)) {
            XWPFDocument document = new XWPFDocument(inputStream);
            PdfOptions options = PdfOptions.create();
            PdfConverter.getInstance().convert(document, out, options);
        } catch (Exception e) {
            logger.error("Word转PDf失败: ", e);
        }



    }
}
